import re
from enum import Enum

import rich
from ansible.plugins.callback import CallbackBase

DOCUMENTATION = """
    author: Roberto Majadas
    name: click_output
    type: stdout
    short_description: Show stdout output in autosd-demos
    description:
        - Show stdout output in autosd-demos
    extends_documentation_fragment:
      - default_callback
    requirements:
      - set as stdout in configuration
"""

FILTERED_TASKS = ["Gathering Facts", "Setting common variables"]
ITEM_FORMAT_PATTERNS = [
    (r"Prepare project required directories", 'Create directory: "$BUILD_DIR/{item}"'),
    (r"Prepare project required files", 'Create file: "$BUILD_DIR/{dest}"'),
    (r"Prepare project required templated files", 'Template file: "$BUILD_DIR/{dest}"'),
    (r"Build custom containers", "Build container: {item}"),
]


class TaskState(Enum):
    OK = 1
    CHANGED = 2
    FAILED = 3
    STARTED = 4
    ITEM_OK = 5
    ITEM_CHANGED = 6
    ITEM_FAILED = 7

    def format_message_with_color(self, message):
        match self:
            case TaskState.OK | TaskState.ITEM_OK:
                return f"[green][🗸][/green] {message}"
            case TaskState.CHANGED | TaskState.ITEM_CHANGED:
                return f"[green][[/green][yellow]🗸[/yellow][green]][/green] {message}"
            case TaskState.FAILED | TaskState.ITEM_FAILED:
                return f"[red][■] {message}[/red]"
            case TaskState.STARTED:  # pragma: no cover
                raise Exception(f"Unexpected state {self}")
            case _:  # pragma: no cover
                raise Exception(f"Unknown state {self}")


class StatusManager:
    def __init__(self, status):
        self.status = status
        if self.status is not None:
            self.status.start()

        self.current_task = None
        self.current_task_items = []

    def update_message(self, state, message, item=None):
        if self.ignore_message(state, message):
            return

        match state:
            case TaskState.OK | TaskState.CHANGED | TaskState.FAILED:
                if not self.current_task_items:
                    formatted_message = state.format_message_with_color(message)
                    rich.print(formatted_message)
                self.current_task = None
                self.current_task_items = []
                self.status.update("")
            case TaskState.STARTED:
                self.current_task = message
                self.status.update(f"{self.current_task}")
            case TaskState.ITEM_OK | TaskState.ITEM_CHANGED | TaskState.ITEM_FAILED:
                if not self.current_task_items:
                    # The first item task, print the main task name.
                    rich.print(self.current_task)
                item_message = self.get_item_formatted_message(message, item)
                formatted_message = state.format_message_with_color(item_message)
                rich.print(f"  {formatted_message}")
                self.current_task_items.append(formatted_message)
                self.status.update("")
            case _:  # pragma: no cover
                raise Exception(f"Unknown state {state}")

    def ignore_message(self, state, message):
        if self.status is None:
            return True

        match state:
            case TaskState.ITEM_OK | TaskState.ITEM_CHANGED | TaskState.ITEM_FAILED:
                return not self.has_item_formatted_messages(message)
            case _:
                return self.message_is_filtered(message)

    @staticmethod
    def message_is_filtered(message):
        return message in FILTERED_TASKS

    @staticmethod
    def has_item_formatted_messages(message):
        return any(re.search(pattern, message) for pattern, _ in ITEM_FORMAT_PATTERNS)

    @staticmethod
    def get_item_formatted_message(message, item):
        for pattern, proposed_message in ITEM_FORMAT_PATTERNS:
            if re.search(pattern, message):
                if isinstance(item, dict):
                    return proposed_message.format(item=item, **item)
                else:
                    return proposed_message.format(item=item)


class CallbackModule(CallbackBase):  # type: ignore[misc]
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = "stdout"
    CALLBACK_NAME = "autosd.demo.click_output"

    CALLBACK_NEEDS_ENABLED = False

    def __init__(self, status=None):
        self.status = StatusManager(status)

    def v2_runner_on_failed(self, result, ignore_errors=False):
        task = getattr(result, "_task")
        task_name = task.get_name()
        self.status.update_message(TaskState.FAILED, task_name)

    def v2_runner_on_ok(self, result):
        task_state = (
            TaskState.CHANGED if result._result.get("changed", False) else TaskState.OK
        )
        task = getattr(result, "_task")
        task_name = task.get_name()
        self.status.update_message(task_state, task_name)

    def v2_runner_item_on_ok(self, result):
        task_state = (
            TaskState.ITEM_CHANGED
            if result._result.get("changed", False)
            else TaskState.ITEM_OK
        )
        task = getattr(result, "_task")
        task_name = task.get_name()
        item_label = self._get_item_label(result._result)
        self.status.update_message(task_state, task_name, item_label)

    def v2_runner_item_on_failed(self, result, ignore_errors=False):
        task = getattr(result, "_task")
        task_name = task.get_name()
        item_label = self._get_item_label(result._result)

        self.status.update_message(TaskState.ITEM_FAILED, task_name, item_label)

    def v2_runner_on_unreachable(self, result):  # pragma: no cover
        pass

    def v2_runner_on_skipped(self, result):  # pragma: no cover
        pass

    def v2_playbook_on_task_start(self, task, is_conditional):
        task_name = task.get_name()
        self.status.update_message(TaskState.STARTED, task_name)
