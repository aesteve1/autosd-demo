import shlex

from click import Context

from autosd_demo.cli import cli as main_cli


class NotebookCLI:
    def __init__(self, cmd=None, subcommands=[], show_hidden=False):
        if cmd is None:
            self._cmd = main_cli
            self._ctx = Context(main_cli)
        else:
            self._cmd = cmd
            self._ctx = Context(self._cmd)
        self._cmd_subcommands = subcommands
        self._show_hidden = show_hidden

    def __dir__(self):
        if self._show_hidden is False:
            cmd_list = []
            for cmd_name in self._cmd.list_commands(self._ctx):
                cmd = self._cmd.get_command(self._ctx, cmd_name)
                if not cmd.hidden:
                    cmd_list.append(cmd_name)
            return cmd_list
        else:
            return self._cmd.list_commands(self._ctx)

    def __getattr__(self, name):
        if name in self._cmd.list_commands(self._ctx):
            cmd = self._cmd.get_command(self._ctx, name)
            subcommands = self._cmd_subcommands + [name]
            return NotebookCLI(cmd, subcommands, show_hidden=self._show_hidden)
        else:
            return None

    def __call__(self, *args, **kwargs):
        try:
            if len(args) == 0:
                main_cli.main(prog_name="autosd-demo", args=self._cmd_subcommands)
            elif len(args) == 1 and isinstance(args[0], list):
                main_cli.main(
                    prog_name="autosd-demo", args=self._cmd_subcommands + args[0]
                )
            elif len(args) == 1 and isinstance(args[0], str):
                cmd_args = shlex.split(args[0])
                main_cli.main(
                    prog_name="autosd-demo", args=self._cmd_subcommands + cmd_args
                )
            else:
                raise ValueError("Invalid arguments")
        except SystemExit as e:
            return e.code
        except ValueError:
            return 1
