from pathlib import Path

from dynaconf import Dynaconf

from autosd_demo import AUTOSD_DEMO_DEFAULT_CONF_PATH, AUTOSD_DEMO_GLOBAL_CONF_FILE
from autosd_demo.utils import find_project_settings, in_autosd_demo
from autosd_demo.utils.addons import get_addons_metadata


class Settings(Dynaconf):  # type: ignore[misc]
    def __init__(self):
        if in_autosd_demo():
            project_settings_file = find_project_settings()
            addons_includes = self._get_addons_preload_includes(project_settings_file)
            super().__init__(
                **self._init_params(project_settings_file, addons_includes)
            )
            self._enrich_container_settings()
            self.unset("GIT_RELEASE_VERSION")
        else:
            super().__init__()

    @classmethod
    def includes(cls):
        return [
            p.as_posix()
            for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "includes").iterdir()
            if p.suffix in [".toml", ".py"]
        ]

    @classmethod
    def preload(cls):
        return [
            p.as_posix()
            for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "preload").iterdir()
            if p.suffix in [".toml", ".py"]
        ]

    @classmethod
    def _init_params(cls, settings_file, extra_preload=None):
        preload = cls.preload()
        if extra_preload:
            preload += extra_preload
        if AUTOSD_DEMO_GLOBAL_CONF_FILE.exists():
            preload.append(AUTOSD_DEMO_GLOBAL_CONF_FILE.as_posix())
        return {
            "root_path": settings_file.parent.as_posix(),
            "includes": sorted(cls.includes()),
            "preload": sorted(preload),
            "secrets": [".secrets.toml"],
            "merge_enabled": True,
            "environments": True,
            "env_switcher": "AUTOSD_DEMO_ENV",
            "envvar_prefix": "AUTOSD_DEMO",
            "settings_files": [settings_file.name],
        }

    @classmethod
    def _get_addons_preload_includes(cls, settings_file):
        pre_settings = Dynaconf(**cls._init_params(settings_file))
        addons_includes = []
        if "addons" in pre_settings:
            addons_metadata = get_addons_metadata()
            for addon_name in pre_settings["addons"]:
                if addon_name not in addons_metadata:
                    continue
                addon_config = (
                    Path(addons_metadata[addon_name]["base_dir"]) / "config.toml"
                )
                # Recursively look for nested addons
                nested_addons = cls._get_addons_preload_includes(addon_config)
                if nested_addons:
                    addons_includes.extend(nested_addons)
                addons_includes.append(str(addon_config.absolute()))
        return addons_includes

    def _enrich_container_settings(self):
        if "containers" not in self:
            return

        for container_name, container_data in self["containers"].items():
            container_defaults = [("version", "latest")]
            for key, value in container_defaults:
                if key not in container_data:
                    self["containers"][container_name][key] = value

        for container_name, container_data in self["containers"].items():
            container_systemd_defaults = [
                ("unit", "description", f"{container_name} container"),
                ("container", "container_name", container_name),
                (
                    "container",
                    "image",
                    f"localhost/{container_name}:" + container_data["version"],
                ),
                ("install", "wanted_by", "multi-user.target"),
                ("service", "restart", "always"),
            ]

            for section, key, value in container_systemd_defaults:
                try:
                    if not container_data["systemd"][section][key]:
                        # A default overwritten by a value that is interpreted
                        # as False. Remove the key from the settings.
                        del self["containers"][container_name]["systemd"][section][key]
                except KeyError:
                    self["containers"][container_name].setdefault(
                        "systemd", {}
                    ).setdefault(section, {})[key] = value


settings = Settings()


def reload_settings():
    global settings
    settings = Settings()
