import getpass
import sys
import tempfile
from pathlib import Path

import ansible
import click
import yaml
from ansible import constants as C
from ansible.cli.playbook import PlaybookCLI
from fabric import Connection
from invoke import AuthFailure, Config, Context
from rich import progress
from rich.status import Status

from autosd_demo.ansible.callbacks.click_output import (
    CallbackModule as ClickOutputCallback,
)
from autosd_demo.cli.exceptions import AnsibleError, BuilderError
from autosd_demo.settings import settings
from autosd_demo.utils.ansible import get_ansible_playbook_path_from_name


class BuilderExecutor:
    TOTAL_LINES_ESTIMATE = 1500

    def run_builder(self, command):
        with self.construct_bar() as pb:
            with self.run(command, **self.run_kwargs()) as process:
                estimated_total = self.TOTAL_LINES_ESTIMATE
                building = pb.add_task(
                    "Build image with automotive-image-builder",
                    total=estimated_total,
                    start=False,
                )
                while not process.runner.stdout:  # pragma: no cover
                    self._check_run_stderr(process.runner.stderr)
                prev_cnt = 0
                pb.start_task(building)
                while not pb.finished:  # pragma: no cover
                    self._check_run_stderr(process.runner.stderr)
                    lines_cnt = len(process.runner.stdout)
                    if prev_cnt == lines_cnt:
                        continue
                    new_line = process.runner.stdout[-1].rstrip()
                    if self.debug:
                        pb.print(new_line)
                    if new_line.endswith("failed"):
                        raise BuilderError(process.runner.stdout)
                    if new_line.endswith("finished successfully"):
                        # Finished the build, finish the progress bar.
                        pb.update(building, completed=estimated_total)
                        pb.stop()
                        break
                    # Reaching the total prematurely, increase the estimation.
                    if estimated_total * 0.995 <= lines_cnt:
                        estimated_total += 50
                        pb.update(building, total=estimated_total)
                    pb.update(building, completed=lines_cnt)
                    prev_cnt = lines_cnt

    def _check_run_stderr(self, stderr):
        if any("error" in line.lower() for line in stderr):  # pragma: no cover
            raise BuilderError(stderr)

    def construct_bar(self):
        spinner_col = progress.SpinnerColumn(
            spinner_name="point",
            finished_text="[green][[/green][yellow]🗸[/yellow][green]][/green]",
        )
        text_col = progress.TextColumn("[progress.description]{task.description}")
        return progress.Progress(
            spinner_col,
            text_col,
            progress.BarColumn(),
            progress.TaskProgressColumn(),
            progress.TimeElapsedColumn(),
        )

    def run_kwargs(self):
        run_kwargs = {
            "warn": True,
            "hide": "out",
            "asynchronous": True,
        }
        if self.debug:
            run_kwargs["echo"] = True
        return run_kwargs


class AnsibleExecutor:
    def run_playbook(self, playbook, extra_vars={}):
        if not self.debug:
            C.DEFAULT_STDOUT_CALLBACK = ClickOutputCallback(status=self.status)

        with tempfile.TemporaryDirectory() as tmpdir:
            ansible_cmd = ["ansible-playbook"]
            if self.debug:
                ansible_cmd += ["-vv"]
            if isinstance(self, Connection):
                ansible_cmd += [
                    "-i",
                    f"{self.host},",
                    "-u",
                    self.user,
                    "-e",
                    f"ansible_ssh_port={self.port}",
                ]
            else:
                ansible_cmd += ["-i", "localhost,", "-e", "ansible_connection=local"]

            if extra_vars != {}:
                extra_vars_path = Path(tmpdir, "extra-vars.yaml")
                ansible_cmd += ["-e", f"@{extra_vars_path.as_posix()}"]

                with open(extra_vars_path, "w") as fd:
                    yaml.dump(dict(extra_vars), fd, default_flow_style=False)

            playbook_path = get_ansible_playbook_path_from_name(playbook)
            ansible_cmd += [playbook_path.as_posix()]

            if hasattr(
                ansible.utils.vars.load_extra_vars, "extra_vars"
            ):  # pragma: no cover
                # Remove ansible extra_vars cache between executions
                del ansible.utils.vars.load_extra_vars.extra_vars

            cli = PlaybookCLI(ansible_cmd)
            cli.ask_passwords = lambda: (None, self.config["sudo"]["password"])
            cli.parse()
            result = cli.run()
            if self.status:
                self.status.stop()  # pragma: no cover
            if result != 0:
                raise AnsibleError(result, playbook)


def ensure_required_access(become_required=False, host_conf=None):
    """Return the credentials."""
    is_remote = host_conf is not None

    if is_remote:
        ctx = Connection(
            host=host_conf["address"], port=host_conf["port"], user=host_conf["user"]
        )
    else:
        ctx = Context()

    if become_required:
        result = ctx.run("sudo -n true", hide="both", warn=True)
        if not result.ok:
            sudo_pass = click.prompt("Sudo password", hide_input=True)
            new_config = ctx.config
            new_config["sudo"]["password"] = sudo_pass
            ctx.config = new_config
            try:
                ctx.sudo("id", hide="both", warn=True)
            except AuthFailure:
                click.echo("sudo password is not valid. Abort!")
                sys.exit(1)

    if is_remote:
        ctx.close()

    return {"sudo": ctx.config["sudo"]["password"], "ssh": None}


def __init_executor__(self, host):
    if isinstance(self, Connection):
        # Remote build.
        host_conf = (
            settings.remote_host.get(host, {}) if "remote_host" in settings else {}
        )
        host_conf.setdefault("address", host)
        host_conf.setdefault("port", 22)
        host_conf.setdefault("user", getpass.getuser())

        credentials = ensure_required_access(
            become_required=self.become_required, host_conf=host_conf
        )
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Connection.__init__(
            self,
            host=host_conf["address"],
            config=new_config,
            port=host_conf["port"],
            user=host_conf["user"],
        )
    else:
        credentials = ensure_required_access(become_required=self.become_required)
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Context.__init__(self, config=new_config)

    AnsibleExecutor.__init__(self)
    BuilderExecutor.__init__(self)


def __enter_executor__(self):
    if isinstance(self, Connection):
        return Connection.__enter__(self)
    return self


def __exit_executor__(self, *exc):
    if isinstance(self, Connection):
        Connection.__exit__(self, *exc)


def get_click_param(param_name, click_context=None):
    if click_context is None:
        click_context = click.get_current_context()
    try:
        return click_context.params[param_name]
    except KeyError:
        if click_context.parent is not None:
            return get_click_param(param_name, click_context.parent)
        raise


def get_executor(remote=False, become_required=False):
    context_class = Connection if remote else Context
    return type(
        "Executor",
        (context_class, AnsibleExecutor, BuilderExecutor),
        {
            "__init__": __init_executor__,
            "__enter__": __enter_executor__,
            "__exit__": __exit_executor__,
            "become_required": become_required,
            "status": Status("", spinner="point"),
            "debug": get_click_param("debug"),
        },
    )
