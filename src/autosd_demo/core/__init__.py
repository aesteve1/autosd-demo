from enum import StrEnum


class Playbooks(StrEnum):
    PRE_BUILD = "pre-build.yaml"
    PROVISION_BUILD = "provision-build.yaml"
    POST_BUILD = "post-build.yaml"
    CLEAN_BUILD = "clean-build.yaml"
    DISABLE_SUDO_PASSWD = "sudo-password-disable.yaml"
    ENABLE_SUDO_PASSWD = "sudo-password-enable.yaml"
    SETUP_DEMO = "setup.yaml"
    SHOW_OSBUILD_MANIFEST = "show-osbuild-manifest.yaml"
    PREPARE_COMPOSE = "prepare-compose.yaml"
