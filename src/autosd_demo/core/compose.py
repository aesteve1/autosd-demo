from enum import StrEnum, auto
from functools import cached_property
from pathlib import Path
from typing import List, Tuple

AUTOSD_DEMO_COMPOSE_CONFIG_EXT = ["yaml", "yml"]


class ComposeSubCmd(StrEnum):
    BUILD = auto()
    UP = auto()
    DOWN = auto()
    CONFIG = auto()


class Compose:
    PODMAN_CMD = "podman compose"

    def __init__(self, build_dir: str):
        self.compose_dir = Path(build_dir, "compose")

    @cached_property
    def cmd(self) -> List[str]:
        return str(self.PODMAN_CMD).split()

    def build(self, options: Tuple[str]) -> str:
        return " ".join(self._run(ComposeSubCmd.BUILD, list(options)))

    def up(self, options: Tuple[str]) -> str:
        return " ".join(self._run(ComposeSubCmd.UP, list(options)))

    def down(self, options: Tuple[str]) -> str:
        return " ".join(self._run(ComposeSubCmd.DOWN, list(options)))

    def config(self, options: Tuple[str]):
        return " ".join(self._run(ComposeSubCmd.CONFIG, list(options)))

    def _config_options(self):
        user_dir = self.compose_dir / "user"
        user_config = []
        for ext in AUTOSD_DEMO_COMPOSE_CONFIG_EXT:
            for file in self.compose_dir.glob(f"*/*.{ext}"):
                if "compose" not in file.stem:
                    continue
                if user_dir in file.parents:
                    user_config.append(file)
                    continue
                yield "-f"
                yield str(file)
        # User defined compose configuration come last.
        for file in user_config:
            yield "-f"
            yield str(file)

    def _run(self, subcmd: ComposeSubCmd, args: List[str]) -> List[str]:
        cmd_cli = self.cmd + list(self._config_options())
        cmd_cli.append(str(subcmd))
        if any(args):
            cmd_cli.extend(args)
        return cmd_cli
