import hashlib
from pathlib import Path

from passlib.handlers.sha2_crypt import sha256_crypt

from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.utils.build import BuildConfig, reduce_directory_list


class OSBuildConfig(BuildConfig):
    def __init__(self, build_files, build_directories):
        super().__init__()
        self.build_files = build_files
        self.build_directories = build_directories
        self._set_config()

    def _set_config(self):
        self._get_preprocessed_containers()
        self._get_preprocessed_users()
        self._get_preprocessed_groups()
        self._get_preprocessed_permissions()
        self._get_preprocessed_systemd()
        self._get_preprocessed_extra_contents()
        self["has_qm"] = any(v for k, v in self.items() if k.startswith("qm_"))
        # mpp_vars are processed last
        self._get_preprocessed_mpp_vars()

    def _get_preprocessed_containers(self):
        osbuild_conf = {"containers": [], "qm_containers": []}

        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                self.conf_required(
                    "containers", container_id, "systemd", "container", "image"
                )
                new_container = {
                    "tag": container_data.get("version", "latest"),
                    "name": container_data["systemd"]["container"]["image"],
                }
                image_name = container_data.get("image", container_id)

                if set(container_data.keys()).intersection(
                    ["container_context", "container_file"]
                ):
                    new_container["source"] = f"localhost/{image_name}"
                    new_container["containers-transport"] = "containers-storage"
                else:
                    source = [
                        container_data.get("registry", None),
                        container_data.get("namespace", None),
                        image_name,
                    ]
                    source = [x for x in source if x is not None]
                    new_container["source"] = "/".join(source)

                if "layer" in container_data and container_data["layer"] == "qm":
                    osbuild_conf["qm_containers"].append(new_container)
                else:
                    osbuild_conf["containers"].append(new_container)

        self.update(osbuild_conf)

    def _get_preprocessed_users(self):
        osbuild_conf = {"users": {}, "qm_users": {}}
        special_params = ["layer", "name"]
        allowed_params = ["uid", "password", "gid", "groups", "home", "description"]
        if self.conf_exists("users"):
            for user_id, user_data in self.conf["users"].items():
                username = user_data["name"] if "name" in user_data else user_id
                new_user = {f"{username}": {}}
                for key, value in user_data.items():
                    if key in special_params:
                        continue
                    if key not in allowed_params:
                        raise ValidationError(
                            "users", user_id, f"option '{key}' is not allowed"
                        )

                    if key == "password":
                        new_user[username][key] = sha256_crypt.hash(value)
                    elif key in ["uid", "gid"]:
                        new_user[username][key] = int(value)
                    elif key == "groups" and not isinstance(value, list):
                        new_user[username][key] = [value]
                    else:
                        new_user[username][key] = value

                users_v = (
                    osbuild_conf["qm_users"]
                    if "layer" in user_data and user_data["layer"] == "qm"
                    else osbuild_conf["users"]
                )
                users_v.update(new_user)

        self.update(osbuild_conf)

    def _get_preprocessed_groups(self):
        osbuild_conf = {"groups": {}, "qm_groups": {}}
        special_params = ["layer"]
        allowed_params = ["gid"]
        if self.conf_exists("groups"):
            for group_id, group_data in self.conf["groups"].items():
                groupname = group_data["name"] if "name" in group_data else group_id
                new_group = {f"{groupname}": {}}
                for key, value in group_data.items():
                    if key in special_params:
                        continue
                    if key not in allowed_params:
                        raise ValidationError(
                            "groups", group_id, f"option '{key}' is not allowed"
                        )

                    if key == "gid":
                        new_group[groupname][key] = int(value)

                groups_v = (
                    osbuild_conf["qm_groups"]
                    if "layer" in group_data and group_data["layer"] == "qm"
                    else osbuild_conf["groups"]
                )
                groups_v.update(new_group)

        self.update(osbuild_conf)

    def _get_preprocessed_permissions(self):
        osbuild_conf = {"chown": {}, "chmod": {}, "qm_chown": {}, "qm_chmod": {}}

        if self.conf_exists("fs_perm"):
            for fs_perm_id, fs_perm_data in self.conf["fs_perm"].items():
                self.conf_required("fs_perm", fs_perm_id, "path")

                (chown_v, chmod_v) = (
                    (osbuild_conf["qm_chown"], osbuild_conf["qm_chmod"])
                    if "layer" in fs_perm_data and fs_perm_data["layer"] == "qm"
                    else (osbuild_conf["chown"], osbuild_conf["chmod"])
                )
                if "user" in fs_perm_data or "group" in fs_perm_data:
                    chown = {}
                    if "user" in fs_perm_data:
                        chown["user"] = fs_perm_data["user"]
                    if "group" in fs_perm_data:
                        chown["group"] = fs_perm_data["group"]
                    chown["recursive"] = fs_perm_data.get("recursive", False)
                    chown_v[fs_perm_data["path"]] = chown
                if "perms" in fs_perm_data:
                    if not isinstance(fs_perm_data["perms"], str):
                        raise ValidationError(
                            "fs_perm", fs_perm_id, "'perms' value must be string"
                        )
                    chmod = {
                        "mode": fs_perm_data["perms"],
                        "recursive": fs_perm_data.get("recursive", False),
                    }
                    chmod_v[fs_perm_data["path"]] = chmod

        self.update(osbuild_conf)

    def _get_preprocessed_systemd(self):
        osbuild_conf = {"systemd": {}, "qm_systemd": {}}
        special_params = ["layer"]
        allowed_params = ["enabled_services", "disabled_services"]
        if self.conf_exists("systemd"):
            for systemd_id, systemd_data in self.conf["systemd"].items():
                systemd_v = (
                    osbuild_conf["qm_systemd"]
                    if "layer" in systemd_data and systemd_data["layer"] == "qm"
                    else osbuild_conf["systemd"]
                )
                for key, value in systemd_data.items():
                    if key in special_params:
                        continue
                    if key not in allowed_params:
                        raise ValidationError(
                            "systemd", systemd_id, f"option '{key}' is not allowed"
                        )

                    systemd_v.setdefault(key, [])
                    if isinstance(value, list):
                        systemd_v[key] = list(set(systemd_v[key] + value))
                    elif isinstance(value, str):
                        if value not in systemd_v[key]:
                            systemd_v[key].append(value)
                    else:
                        raise ValidationError(
                            "systemd",
                            systemd_id,
                            f"'{key}' value must be string or list",
                        )
                    systemd_v[key].sort()

        self.update(osbuild_conf)

    def _get_preprocessed_extra_contents(self):
        osbuild_conf = {"extra_contents": {}, "qm_extra_contents": {}}

        self._preprocess_extra_contents_files(osbuild_conf)
        self._preprocess_extra_contents_directories(osbuild_conf)
        self._finalize_extra_contents(osbuild_conf)

        self.update(osbuild_conf)

    def _preprocess_extra_contents_files(self, osbuild_conf):
        for item in self.build_files:
            dest_path = Path(item["dest"])
            if dest_path.parts[2] != "extra_files":
                continue
            layer = dest_path.parts[3].split("_")[0]

            extra_contents_v = (
                osbuild_conf["qm_extra_contents"]
                if layer == "qm"
                else osbuild_conf["extra_contents"]
            )

            item_idx = extra_contents_v.setdefault("index", 0)
            content_id = hashlib.sha256(
                f"{item.get('src', '')}\n{item['dest']}".encode()
            ).hexdigest()[:7]
            tree_dest_path = dest_path.relative_to(Path(*dest_path.parts[:4]))

            ec_input = {
                f"{layer}_extra_content_{item_idx}": {
                    "type": "org.osbuild.files",
                    "origin": "org.osbuild.source",
                    "mpp-embed": {
                        "id": f"{content_id}",
                        "path": Path("..").joinpath(dest_path).as_posix(),
                    },
                }
            }

            ec_path = {
                "from": {
                    "mpp-format-string": (
                        f"input://{layer}_extra_content_{item_idx}/{{embedded['{content_id}']}}"
                    )
                },
                "to": f"tree:///{tree_dest_path.as_posix()}",
            }

            extra_contents_v.setdefault("inputs", {}).update(ec_input)
            extra_contents_v.setdefault("paths", []).append(ec_path)
            extra_contents_v.setdefault("directories", []).append(
                "/" + tree_dest_path.parent.as_posix()
            )
            extra_contents_v["index"] += 1

    def _preprocess_extra_contents_directories(self, osbuild_conf):
        for directory in self.build_directories:
            dir_path = Path(directory)
            if dir_path.parts[2] != "extra_files":
                continue
            layer = dir_path.parts[3].split("_")[0]

            extra_contents_v = (
                osbuild_conf["qm_extra_contents"]
                if layer == "qm"
                else osbuild_conf["extra_contents"]
            )

            tree_dest_path = dir_path.relative_to(Path(*dir_path.parts[:4]))
            extra_contents_v.setdefault("inputs", {})
            extra_contents_v.setdefault("paths", [])
            extra_contents_v.setdefault("directories", []).append(
                "/" + tree_dest_path.as_posix()
            )

    def _finalize_extra_contents(self, osbuild_conf):
        for extra_contents_v in [
            osbuild_conf["extra_contents"],
            osbuild_conf["qm_extra_contents"],
        ]:
            if "index" in extra_contents_v:
                extra_contents_v.pop("index")

            if "directories" in extra_contents_v:
                extra_contents_v["directories"] = [
                    {"path": d, "exist_ok": True, "parents": True}
                    for d in reduce_directory_list(extra_contents_v["directories"])
                ]

    def _get_preprocessed_mpp_vars(self):
        osbuild_conf = {"mpp_vars": self.conf.get("mpp_vars", {})}
        if self.conf_exists("rpm"):
            osbuild_conf["mpp_vars"].update(self._get_preprocessed_rpms())
        if self.conf_exists("rpm_repo"):
            osbuild_conf["mpp_vars"].update(self._get_preprocessed_rpm_repos())
        if self.conf_exists("dracut"):
            osbuild_conf["mpp_vars"].update(self._get_preprocessed_dracut_conf())
        if self.get("containers", False):
            osbuild_conf["mpp_vars"].update({"use_containers_extra_store": True})
        if self.get("qm_containers", False):
            osbuild_conf["mpp_vars"].update({"use_qm_containers_extra_store": True})
        if not self.get("has_qm", False):
            self["has_qm"] = any(k.startswith("qm_") for k in osbuild_conf["mpp_vars"])
        if self["has_qm"]:
            osbuild_conf["mpp_vars"].update({"use_qm": True})
            osbuild_conf["mpp_vars"].update({"use_bluechi": True})
        self.update(osbuild_conf)

    def _get_preprocessed_dracut_conf(self):
        osbuild_conf = {}
        for dracut_data in self.conf["dracut"].values():
            if "modules" in dracut_data:
                osbuild_conf.setdefault("dracut_add_modules", []).extend(
                    dracut_data["modules"]
                )
            if "install" in dracut_data:
                osbuild_conf.setdefault("dracut_install", []).extend(
                    dracut_data["install"]
                )
        return osbuild_conf

    def _get_preprocessed_rpm_repos(self):
        osbuild_conf = {}
        for rpm_repo_id, rpm_repo_data in self.conf["rpm_repo"].items():
            self.conf_required("rpm_repo", rpm_repo_id, "baseurl")

            new_repo = {"id": rpm_repo_id, "baseurl": rpm_repo_data["baseurl"]}

            layer_key = "extra_repos"
            if rpm_repo_data.get("layer") == "qm":
                layer_key = f"qm_{layer_key}"
            osbuild_conf.setdefault(layer_key, []).append(new_repo)

        return osbuild_conf

    def _get_preprocessed_rpms(self):
        osbuild_conf = {}
        for rpm_id, rpm_data in self.conf["rpm"].items():
            self.conf_required("rpm", rpm_id, "packages")

            layer_key = "extra_rpms"
            if rpm_data.get("layer") == "qm":
                layer_key = f"qm_{layer_key}"
            osbuild_conf.setdefault(layer_key, []).extend(rpm_data["packages"])

        return osbuild_conf
