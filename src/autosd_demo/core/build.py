import re
from functools import cached_property
from pathlib import Path

from jinja2 import Template

from autosd_demo.core.osbuild import OSBuildConfig
from autosd_demo.utils import in_autosd_demo
from autosd_demo.utils.build import BuildConfig, reduce_directory_list


class BuildEnv(BuildConfig):
    LINK_PATTERN = re.compile(r"^[a-z]+://")

    def __init__(self, host, osbuild=False):
        super().__init__()
        self.host = host
        self._set_build_env_config()
        if osbuild:
            self.update(
                {
                    "osbuild": dict(
                        OSBuildConfig(self.build_files, self.build_directories)
                    )
                }
            )

    @property
    def is_remote(self):
        return self.host is not None

    @cached_property
    def base_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["base_dir"]
                if self.conf_exists("remote_host", self.host, "base_dir")
                else "~"
            )

        return self.conf["base_dir"]

    @property
    def build_id(self):
        return self["build"]["build_id"]

    @property
    def hooks(self):
        return self["hooks"]

    @cached_property
    def build_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["build_dir"]
                if self.conf_exists("remote_host", self.host, "build_dir")
                else str(Path(self.base_dir) / "build")
            )

        return self.conf["build_dir"]

    @cached_property
    def build_files(self):
        extra_contents_list = self["build"]["src"]["files"]
        extra_contents_list += self["build"]["src"]["templated_files"]
        return sorted(extra_contents_list, key=lambda x: x["dest"])

    @cached_property
    def build_directories(self):
        return self["build"]["src"].get("directories", [])

    def _set_build_env_config(self):
        build_env = {"conf": self.conf, "build": {}, "hooks": {}}
        build_env["build"]["is_remote"] = self.is_remote
        if in_autosd_demo():
            build_env["build"]["base_dir"] = self.base_dir
            build_env["build"]["build_dir"] = self.build_dir
            build_env["build"]["src"] = self._get_build_src_contents()
            custom_containers_ids = self._get_build_custom_containers_ids()
            build_env["build"]["custom_containers"] = custom_containers_ids
            build_env["build"]["podman_extra_args"] = self._get_podman_extra_args()
            build_env["hooks"] = self._get_configured_hooks()
        self.update(build_env)

    def _get_configured_hooks(self):
        hooks = {"pre_build": [], "post_build": [], "clean_build": [], "setup": []}
        if self.conf_exists("hooks"):
            for hook_data in self.conf["hooks"].values():
                stages = ["setup", "pre_build", "post_build", "clean_build"]
                for stage in stages:
                    if stage in hook_data:
                        for hook in hook_data[stage]:
                            hook_path = (
                                hook
                                if Path(hook).is_absolute()
                                else str(Path(self.base_dir) / hook)
                            )
                            hooks[stage].append(hook_path)

        for stage in hooks.keys():
            hooks[stage] = sorted(hooks[stage], key=lambda path: Path(path).name)

        return hooks

    def reload_src_contents(self):
        self["build"]["src"] = self._get_build_src_contents()

    def _get_build_src_contents(self):
        src_contents = {
            "directories": set(),
            "files": [],
            "templated_files": [],
            "fetch_files": [],
        }

        self._get_build_src_quadlets(src_contents)
        self._get_build_src_extra_files(src_contents)
        self._get_build_src_custom_containers_files(src_contents)

        src_contents["directories"] = reduce_directory_list(src_contents["directories"])

        return src_contents

    def _get_build_custom_containers_ids(self):
        result = []
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if (
                    "container_context" in container_data
                    or "container_file" in container_data
                ):
                    result.append(container_id)
        return result

    def _get_build_src_custom_containers_files(self, src_contents):
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if "container_context" in container_data:
                    context_path = Path(container_data["container_context"])
                    if not context_path.is_absolute():
                        context_path = Path(self.conf["base_dir"]) / context_path

                    list_files = [f for f in context_path.glob("**/*") if f.is_file()]

                    for file in list_files:
                        path_template = Template(
                            file.as_posix(),
                            variable_start_string="{",
                            variable_end_string="}",
                        )
                        src = Path(path_template.render(conf=self.conf))
                        dest = Path("files") / self.conf["name"]
                        dest = dest / "containers" / container_id
                        dest = dest / src.relative_to(context_path)

                        if file.suffix == ".j2":
                            src_contents["templated_files"].append(
                                {
                                    "src": file.as_posix(),
                                    "dest": dest.with_suffix("").as_posix(),
                                }
                            )
                        else:
                            src_contents["files"].append(
                                {"src": file.as_posix(), "dest": dest.as_posix()}
                            )
                        src_contents["directories"].add(dest.parent.as_posix())

        return src_contents

    def _get_build_src_extra_files(self, src_contents):
        if self.conf_exists("extra_files"):
            for ef_id, ef_data in self.conf["extra_files"].items():
                touch = ef_data.get("touch", False)
                if not touch:
                    self.conf_required("extra_files", ef_id, "src")
                self.conf_required("extra_files", ef_id, "dest")

                dest_path = (
                    Path(ef_data["dest"][1:])
                    if Path(ef_data["dest"]).is_absolute()
                    else Path(ef_data["dest"])
                )
                if touch:
                    self._get_build_src_extra_files_touch(
                        ef_data, dest_path, src_contents
                    )
                elif self.LINK_PATTERN.match(ef_data["src"]):
                    self._get_build_src_extra_files_link(
                        ef_data, dest_path, src_contents
                    )
                else:
                    self._get_build_src_extra_files_regular(
                        ef_data, dest_path, src_contents
                    )

    def _get_build_src_extra_files_touch(self, ef_data, dest_path, src_contents):
        layer = ef_data.get("layer", "root")

        dest = Path("files") / self.conf["name"]
        dest = dest / "extra_files" / f"{layer}_fs"
        dest = dest / dest_path

        if ef_data["dest"].endswith("/"):
            src_contents["directories"].add(dest.as_posix())
        else:
            src_contents["files"].append({"dest": dest.as_posix()})
            src_contents["directories"].add(dest.parent.as_posix())

    def _get_build_src_extra_files_link(self, ef_data, dest_path, src_contents):
        layer = ef_data.get("layer", "root")

        url = ef_data["src"]
        checksum = ef_data["checksum"] if "checksum" in ef_data else ""
        file_name = Path(url.replace("://", "_").replace("/", "_"))
        src = Path(self.conf["base_dir"]) / "cache" / "downloads" / file_name
        dest = (
            Path("files")
            / self.conf["name"]
            / "extra_files"
            / f"{layer}_fs"
            / dest_path
        )
        if ef_data["dest"].endswith("/"):
            dest_name = url.rpartition("/")[2]
            dest = dest / dest_name

        src_contents["fetch_files"].append(
            {"url": url, "checksum": checksum, "file": file_name.as_posix()}
        )
        src_contents["files"].append({"src": src.as_posix(), "dest": dest.as_posix()})
        src_contents["directories"].add(dest.parent.as_posix())

    def _get_build_src_extra_files_regular(self, ef_data, dest_path, src_contents):
        layer = ef_data.get("layer", "root")
        src_path = Path(ef_data["src"])

        if not src_path.is_absolute():
            src_path = Path(self.conf["base_dir"]) / src_path

        if src_path.is_file():
            list_files = [src_path]
        elif src_path.is_dir():
            list_files = [f for f in src_path.glob("**/*") if f.is_file()]
        else:
            return

        for file in list_files:
            path_template = Template(
                file.as_posix(),
                variable_start_string="{",
                variable_end_string="}",
            )
            src = Path(path_template.render(conf=self.conf))
            dest = Path("files") / self.conf["name"]
            dest = dest / "extra_files" / f"{layer}_fs"
            dest = dest / dest_path

            if len(list_files) == 1 and ef_data["dest"].endswith("/"):
                dest = dest / src.name
            else:
                dest = dest / src.relative_to(src_path)

            if file.suffix == ".j2":
                if (
                    len(list_files) > 1
                    or not src_path.is_file()
                    or ef_data["dest"].endswith("/")
                ):
                    dest = dest.with_suffix("")
                src_contents["templated_files"].append(
                    {
                        "src": file.as_posix(),
                        "dest": dest.as_posix(),
                    }
                )
            else:
                src_contents["files"].append(
                    {"src": file.as_posix(), "dest": dest.as_posix()}
                )
            src_contents["directories"].add(dest.parent.as_posix())

    def _get_build_src_quadlets(self, src_contents):
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if not container_data.get("quadlet", True):
                    continue

                layer = (
                    "root" if "layer" not in container_data else container_data["layer"]
                )
                quadlet_dest = (
                    Path("files") / self.conf["name"] / "extra_files" / f"{layer}_fs"
                )
                quadlet_dest = (
                    quadlet_dest / f"etc/containers/systemd/{container_id}.container"
                )

                quadlet_template = {
                    "src": self.conf["centos_sig"]["autosd_demo"][
                        "template_systemd_container_path"
                    ],
                    "dest": quadlet_dest.as_posix(),
                    "container_name": container_id,
                }

                src_contents["templated_files"].append(quadlet_template)
                src_contents["directories"].add(quadlet_dest.parent.as_posix())

    def _get_podman_extra_args(self):
        extra_args = {}
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if "build_args" in container_data:
                    extra_args[container_id] = [
                        f"--build-arg {key}={value}"
                        for key, value in container_data["build_args"].items()
                    ]
        return extra_args
