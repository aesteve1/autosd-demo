import shutil
from datetime import datetime
from pathlib import Path

import click

from autosd_demo.cli.exceptions import catch_exception
from autosd_demo.core import Playbooks
from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor
from autosd_demo.utils.export import create_exported_tarball


@click.command()
@catch_exception
def export():
    """Export tarball for the sample-images repo"""
    build_env = BuildEnv(None, osbuild=True)
    build_env["build"].update(
        {"build_id": f"{datetime.now():%Y%m%d_%H%M%S}", "export": True}
    )
    executor = get_executor()
    with executor(host=None) as ctx:
        ctx.run_playbook(Playbooks.PRE_BUILD, extra_vars=build_env)
        if len(build_env.hooks["pre_build"]) > 0:
            build_env.reload_src_contents()
        ctx.run_playbook(Playbooks.PROVISION_BUILD, extra_vars=build_env)

        build_path = Path(build_env.build_dir, build_env.build_id)
        create_exported_tarball(build_path)
        shutil.rmtree(build_path)
