import click

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception


@click.command
@catch_exception
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command                   ",
    default=None,
)
@click.option(
    "--upgrade",
    is_flag=True,
    default=False,
    help="Upgrade the installed packages to the latest version",
)
def setup(host, upgrade):
    """Setup autosd-demo project environment"""
    build_env = BuildEnv(host)
    build_env["pkgs_state"] = "latest" if upgrade else "present"

    executor = get_executor(remote=build_env.is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.SETUP_DEMO, extra_vars=build_env)
