from functools import cached_property
from pathlib import Path

import click

from autosd_demo import utils

EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT = {"new"}
EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT = {"build", "compose", "export", "show"}


class MainCLIGroup(click.Group):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @cached_property
    def cli_commands(self):
        commands = set()
        for cmd_file in Path(__file__).parent.glob("cmd_*.py"):
            mod_name = cmd_file.with_suffix("").name
            cmd_name = mod_name[4:]
            commands.add(cmd_name)
        return commands

    def list_commands(self, ctx):
        excluded_commands = (
            EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT
            if utils.in_autosd_demo()
            else EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT
        )
        return sorted(self.cli_commands - excluded_commands)

    def get_command(self, ctx, cmd_name):
        if cmd_name not in self.cli_commands:
            return None
        if cmd_name not in self.list_commands(ctx):
            location = "inside" if utils.in_autosd_demo() else "outside of"
            raise click.ClickException(
                f"Command '{cmd_name}' is not allowed {location} autosd-demo project. "
                "Reference '--help' for a list of available commands."
            )

        try:
            mod = __import__(f"autosd_demo.cli.cmd_{cmd_name}", None, None, [cmd_name])
        except ImportError:  # pragma: no cover
            return None
        return getattr(mod, cmd_name)


@click.group(cls=MainCLIGroup)
@click.pass_context
@click.option(
    "--debug",
    is_flag=True,
    help="Display Ansible error details",
    default=False,
)
def cli(click_context, debug):
    click_context.params["debug"] = debug
