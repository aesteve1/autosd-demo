from datetime import datetime
from enum import StrEnum, auto, unique
from pathlib import Path

import click

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from ..settings import settings
from .exceptions import AnsibleError, catch_exception


@unique
class ExportType(StrEnum):
    QCOW2 = auto()
    IMAGE = auto()
    OSTREE_COMMIT = "ostree-commit"
    CONTAINER = auto()
    OSTREE_OCI_IMAGE = "ostree-oci-image"
    EXT4 = auto()
    TAR = auto()
    ABOOT = auto()
    RPMLIST = auto()
    EXT4_SIMG = "ext4.simg"
    SIMG = auto()
    ABOOT_SIMG = "aboot.simg"


EXPORT_EXT = {
    ExportType.QCOW2: "qcow2",
    ExportType.IMAGE: "img",
    ExportType.OSTREE_COMMIT: "repo",
    ExportType.CONTAINER: "tar",
    ExportType.OSTREE_OCI_IMAGE: "oci-archive",
    ExportType.EXT4: "ext4",
    ExportType.TAR: "tar",
    ExportType.ABOOT: "images",
    ExportType.RPMLIST: "rpmlist",
    ExportType.EXT4_SIMG: "ext4",
    ExportType.SIMG: "img",
    ExportType.ABOOT_SIMG: "images",
}


@click.command
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command          ",
    default=None,
)
@click.option(
    "-d",
    "--distro",
    help="Distro used as image base to build the demo",
    default=settings.get("build.distro", "autosd"),
    show_default=True,
)
@click.option(
    "-t",
    "--target",
    help="Target used to build the demo              ",
    default=settings.get("build.target", "qemu"),
    show_default=True,
)
@click.option(
    "-i",
    "--image-mode",
    help="Image mode used to build the demo          ",
    type=click.Choice(["package", "image"]),
    default=settings.get("build.image_mode", "package"),
    show_default=True,
)
@click.option(
    "-a",
    "--image-arch",
    help="Image architecture                         ",
    default=settings.get("build.image_arch", "x86_64"),
    show_default=True,
)
@click.option(
    "-e",
    "--export",
    help="Export this image type                     ",
    type=click.Choice([e.value for e in ExportType]),
    default=settings.get("build.export", ExportType.QCOW2),
    show_default=True,
)
@click.option(
    "--define",
    help="Define osbuild key=yaml-value              ",
    multiple=True,
)
@click.option(
    "--ostree-repo",
    help="Path to ostree repo                        ",
    type=click.Path(),
    default=settings.get("build.ostree_repo", None),
)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=f"{datetime.now():%Y%m%d_%H%M%S}",
)
@click.option(
    "--no-clean",
    "clean",
    is_flag=True,
    help="Skip build folder cleanup after the build  ",
    default=True,
)
@click.option(
    "-v",
    "--verbose",
    is_flag=True,
    help="Increase image builder verbosity           ",
    default=False,
)
@catch_exception
def build(
    host,
    distro,
    target,
    image_mode,
    image_arch,
    export,
    define,
    ostree_repo,
    build_id,
    clean,
    verbose,
):
    """Build the autosd-demo project"""
    build_env = BuildEnv(host, osbuild=True)

    env_suffix = ""
    if settings.current_env != "DEVELOPMENT":
        env_suffix = f"-{settings.current_env.lower()}"

    build_env["build"].update(
        {
            "build_id": build_id,
            "distro": distro,
            "target": target,
            "image_mode": image_mode,
            "image_arch": image_arch,
            "image_name": (
                f"{distro}-{target}-{build_env['conf']['name']}"
                f"-{image_mode}{env_suffix}.{image_arch}.{EXPORT_EXT[export]}"
            ),
        }
    )

    executor = get_executor(remote=build_env.is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.DISABLE_SUDO_PASSWD, extra_vars=build_env)
        try:
            ctx.run_playbook(Playbooks.PRE_BUILD, extra_vars=build_env)
            if len(build_env.hooks["pre_build"]) > 0:
                build_env.reload_src_contents()
            ctx.run_playbook(Playbooks.PROVISION_BUILD, extra_vars=build_env)
        except AnsibleError:
            if clean:
                ctx.run_playbook(Playbooks.CLEAN_BUILD, extra_vars=build_env)
            ctx.run_playbook(Playbooks.ENABLE_SUDO_PASSWD, extra_vars=build_env)
            raise

        src_dir = Path(build_env.build_dir, build_env.build_id, "src")
        registry_auth_path = Path(
            build_env.build_dir,
            build_env.build_id,
            "auth.json",
        )
        c_storage_conf_path = Path(
            build_env.build_dir,
            build_env.build_id,
            ".containers",
            "storage.conf",
        )
        c_containers_conf_path = Path(
            build_env.build_dir,
            build_env.build_id,
            ".containers",
            "containers.conf",
        )

        defines = settings.get("build.defines", []) + list(define)

        builder_cmd = "/usr/bin/automotive-image-builder"
        if verbose:
            builder_cmd += " --verbose"
        try:
            ctx.run_builder(
                f"REGISTRY_AUTH_FILE=$(realpath {registry_auth_path.as_posix()}) "
                f"CONTAINERS_CONF=$(realpath {c_containers_conf_path.as_posix()}) "
                f"CONTAINERS_STORAGE_CONF=$(realpath {c_storage_conf_path.as_posix()}) "
                f"{builder_cmd} build --distro {distro} --target {target} "
                f"--mode {image_mode} --arch {image_arch} "
                f"--build-dir {src_dir.as_posix()}/_build --cache-max-size=1GB "
                f"--mpp-arg=--cache "
                f"--mpp-arg {src_dir.as_posix()}/_build/mpp_cache "
                f"--export {export} "
                f"{' '.join([f'--define {d}' for d in defines])} "
                f"{f'--ostree-repo {ostree_repo} ' if ostree_repo else ''}"
                f"{src_dir.as_posix()}/images/{build_env.conf['name']}.mpp.yml "
                f"{src_dir.as_posix()}/{build_env['build']['image_name']}"
            )
            ctx.run_playbook(Playbooks.POST_BUILD, extra_vars=build_env)
        finally:
            if clean:
                ctx.run_playbook(Playbooks.CLEAN_BUILD, extra_vars=build_env)
            ctx.run_playbook(Playbooks.ENABLE_SUDO_PASSWD, extra_vars=build_env)
