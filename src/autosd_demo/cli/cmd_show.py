import tempfile
from pathlib import Path

import click
from dynaconf import loaders

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor
from autosd_demo.settings import settings

from ..core import Playbooks
from .exceptions import catch_exception


@click.group()
def show():
    """Show information about the autosd-demo project"""
    pass


@show.command()
@click.option(
    "-o",
    "--output-format",
    default="toml",
    help="Output format",
    type=click.Choice(["toml", "json", "yaml"]),
)
def config(output_format):
    """Show the autosd-demo project's configuration"""
    with tempfile.TemporaryDirectory() as tmpdir:
        exported_file = tmpdir + "/export"
        data = {key.lower(): val for key, val in settings.as_dict().items()}

        if output_format == "json":
            loaders.json_loader.write(exported_file, data)
        elif output_format == "yaml":
            loaders.yaml_loader.write(exported_file, data)
        else:
            loaders.toml_loader.write(exported_file, data)

        with open(exported_file) as fd:
            print(fd.read())


@show.command(name="osbuild-manifest")
@catch_exception
def osbuild_manifest():
    """Show the osbuild manifest rendered"""
    with tempfile.TemporaryDirectory() as tmpdir:
        build_env = BuildEnv(None, osbuild=True)
        build_env["build"].update({"tmp_dir": tmpdir})

        executor = get_executor()
        with executor(host=None) as ctx:
            ctx.run_playbook(Playbooks.SHOW_OSBUILD_MANIFEST, extra_vars=build_env)

        with open(Path(tmpdir, "image.mpp.yml")) as fd:
            print(fd.read())
