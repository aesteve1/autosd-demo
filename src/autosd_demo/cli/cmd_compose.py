import click

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.compose import Compose
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception

SERVICES = ["registry"]


@click.group()
def compose():
    """Run container compose commands"""


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def build(host, opts):
    """Build stack images"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.PREPARE_COMPOSE, extra_vars=build_env)
        cmd = run_compose.build(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def up(host, opts):
    """Create and start the entire stack or some of its services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.PREPARE_COMPOSE, extra_vars=build_env)
        cmd = run_compose.up(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def down(host, opts):
    """Stop the entire stack or some of its services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.PREPARE_COMPOSE, extra_vars=build_env)
        cmd = run_compose.down(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def config(host, opts):
    """Display the resulting compose file with all included services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        ctx.run_playbook(Playbooks.PREPARE_COMPOSE, extra_vars=build_env)
        cmd = run_compose.config(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)
