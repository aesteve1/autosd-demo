import importlib.metadata
import os

__version__ = importlib.metadata.version("autosd-demo")

import sys
from pathlib import Path

from .utils import in_virtualenv

AUTOSD_DEMO_DATA_PATH = Path(sys.prefix, "share", "autosd-demo")
AUTOSD_DEMO_COMPOSE_PATH = AUTOSD_DEMO_DATA_PATH.joinpath("compose")

if in_virtualenv():  # pragma: no cover
    dev_data_path = Path(sys.prefix).parent.joinpath("data", "share", "autosd-demo")
    if dev_data_path.exists():
        AUTOSD_DEMO_DATA_PATH = dev_data_path
        if dev_data_path.joinpath("compose").exists():
            AUTOSD_DEMO_COMPOSE_PATH = dev_data_path.joinpath("compose")

if "RPM_CHECK" in os.environ:  # pragma: no cover
    AUTOSD_DEMO_DATA_PATH = Path(__file__).parents[4].joinpath("share", "autosd-demo")
    AUTOSD_DEMO_COMPOSE_PATH = AUTOSD_DEMO_DATA_PATH.joinpath("compose")

AUTOSD_DEMO_DEFAULT_CONF_PATH = Path(AUTOSD_DEMO_DATA_PATH, "conf")
AUTOSD_DEMO_LOCAL_CONF_FILENAME = "autosd-demo.toml"
AUTOSD_DEMO_GLOBAL_CONF_FILENAME = ".autosd-demo.toml"
AUTOSD_DEMO_GLOBAL_CONF_FILE = Path.home().joinpath(AUTOSD_DEMO_GLOBAL_CONF_FILENAME)
