"""
Installs Wayland session infrastructure. This addon is made separate from
mutter, so it can be used with weston and other addons.
"""
