Quick Start
***********

This is a quick start guide to help you get up and running with the `autosd-demo` tool.

Step 1: Install `autosd-demo`
=============================

You can read about installation details in the :doc:`Installation <install>` manual.

Once `autosd-demo` is installed, you can run it in your terminal.

Context-aware `--help` command is available.

- Show all the available commands:

.. code-block:: bash

   $ autosd-demo --help


- Show all the available options of the specific command:

.. code-block:: bash

   $ autosd-demo build --help

Step 2: Setup Your Environment
==============================

To make sure all requirements are installed in your system:

.. code-block:: bash

   $ autosd-demo setup

This will install the latest version of `automotive-image-builder` and other required tools.

Step 3: Create a New Demo
=========================

To create a new demo, execute the following command:

.. code-block:: bash

   $ autosd-demo new mydemo
   $ cd mydemo

The name of your new demo and the newly created folder will be "`mydemo`".
After execution, an "`.autosd-demo.toml`" configuration file will be created in the new folder.

.. code-block:: toml

    [default]
    name="mydemo"
    version="0.0.1"

Additionally, you can pass the `--git` argument to the `new` command to create the `autosd-demo` project as an empty `Git` repository.

Step 4: Add Your First Container
================================

Add the following information to the "`.autosd-demo.toml`" file:

.. code-block:: toml

    [default.containers.my_container]
    registry="quay.io"
    namespace="myproject"
    image="myapp"
    version="latest"

The `autosd-demo` tool will generate the necessary Quadlet file.

Step 5: Build Your Demo Image
=============================

To build the demo image:

.. code-block:: bash

   $ autosd-demo build

If successful, this will build an "`autosd-qemu-mydemo-package.x86_64.qcow2`" demo image in the current directory.

Notes:

- The "`mydemo`" part (in the filename of the image) is the name of the demo project you created.
- If built with "`AUTOSD_DEMO_ENV`" environment variable (i.e., "`AUTOSD_DEMO_ENV=prod`", see :ref:`using-environments` for more details), the filename will also contain the environment value in its name (i.e., "`autosd-qemu-mydemo-package-prod.x86_64.qcow2`").
- There is an option to build the image remotely, see :ref:`remote-building` for more details.

Step 6: Run Your Demo Image
===========================

To run your demo image:

.. code-block:: bash

   $ automotive-image-runner autosd-qemu-mydemo-package.x86_64.qcow2

Step 7: Iterate!
================

The `autosd-demo` tool offers multiple features for you to customize your demo further:

- Multiple Environments: you can create and manage multiple environment configurations for different purposes and scenarios.
- Remote Building: `autosd-demo` supports building your demo in a remote environment, enabling more flexibility and resource utilization.
- Custom Containers: you can define a container image in a project folder. The `autosd-demo` tool will build this image and ensure it's installed in the demo image.
- RPM Packages and Repositories: `autosd-demo` allows you to specify RPM packages and repositories that will be installed directly into the demo image.
- Templated Files: `autosd-demo` allows you to define template files that use `autosd-demo` configuration variables. Once the template is processed, `autosd-demo` places the resulting files in the demo image at locations specified by you.
- Emulating External Services: `autosd-demo` can emulate external services using Podman compose files or by enabling predefined compose services that `autosd-demo` offers.
- Addons System: `autosd-demo` provides an addons system that allows you to easily extend your demos by reusing components from common scenarios.
- Demo Export: `autosd-demo` allows the export of demos to `osbuild` manifests and packages all the required assets to be used by `automotive-image-builder`.

For more details on these and other features, refer to the full documentation or specific feature documentation in the `docs` directory.
