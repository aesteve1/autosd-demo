.. autosd-demo documentation master file, created by
   sphinx-quickstart on Mon Mar 11 13:45:42 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to `autosd-demo`'s documentation!
=========================================

AutoSD demo is a CLI tool for creating new AutoSD demos.

It was designed to be an easy ramp-up for both customers and developers.

- AutoSD images building
    - Remote building for different architectures
- Additional external services required by the demo, for instance:
    - Local container registries
    - External APIs (i.e., Traffic generator)
- Helpers to manage the demo easily

Examples of how it can help:

- If you want to try RHIVOS for a demo with your own containers,
  you can download `autosd-demo` and create your own demo by filling a configuration file.
- If you want to compile it for `aarch64` and add some external services outside the board,
  you can rebuild the image remotely from your laptop in an `aarch64` cloud instance, and you could add your demo services as a podman compose.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install.rst
   quick-start.rst
   documentation.rst
