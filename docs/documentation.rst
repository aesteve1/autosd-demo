Documentation
*************

Introduction
============
This manual is a comprehensive guide on using **autosd-demo**, a valuable tool for creating new AutoSD demos.

This document covers how to configure an `autosd-demo` project using the configuration file.

Global Configuration
====================

The `autosd-demo` configuration supports two main levels - global and local configurations.
First, it checks for a global file at "`~/.autosd-demo.toml`", which is user-specific.
Then, the `autosd-demo` tool searches for "`autosd-demo.toml`" file in the current project root folder
for the project-specific configuration.

.. note::
    1. Take into account that the local configuration overrides the global one.
    2. The settings will only be loaded if the tool is run from within the `autosd-demo` project,
       even if the global configuration available.

Environments
============

The `autosd-demo` configuration file allows utilizing multiple environments, enabling users to define, add and overwrite configurations.
These environments are defined in the TOML file as the main sections like `[default]`, `[debug]`, `[local]`, `[devel]`, `[prod]`, or any other custom environment.
The `[default]` is the base environment, with all other environments inheriting their configurations from it.

An example of a multi-environment configuration demo is illustrated below:

.. code-block:: toml

    [default]
    name="mydemo"
    version="0.0.1"

    [devel]
    version="0.0.1-dev"

    [local]
    debug=true

Why multiple environments
-------------------------

There are common scenarios where having multiple environments is beneficial including:

- Defining a debug environment to enable debugging flags.
- Defining environments for specific hardware platforms or boards, allowing users to specify configurations for those environments.

.. _using-environments:

Using environments
------------------
To enable a specific environment, you need to export the "`AUTOSD_DEMO_ENV`" environment variable in your shell with the name of the desired environment as its value.
Once this variable is exported, `autosd-demo` will use the configuration defined by this environment.

.. code-block:: bash

    $ AUTOSD_DEMO_ENV=devel autosd-demo build

Environment variables
---------------------

Every configuration key is modifiable via an environment variable.
Consequently, you can modify any aspect of your configuration without changing your configuration file.

For example, if you want to change the `version` field without modifying your TOML file, you would simply export the "`AUTOSD_DEMO__VERSION`" environment variable with a different version number.

.. code-block:: bash

    $ export AUTOSD_DEMO__VERSION=1.0.0
    # The 'version' field is now set to '1.0.0'


Containers
==========

AutoSD operates different applications using containers.
These containers are included in the image and managed by `systemd` as Quadlets.

To facilitate this, `autosd-demo` automatically generates a Quadlet file for each application.
This eliminates the need for manual Quadlet file creation, making the overall process more efficient and user-friendly.


Include a Container from a Container Registry
---------------------------------------------

Including a container from a container registry in `autosd-demo` can be achieved with minimal configuration.
Here is an example:

.. code-block:: toml

    [default.containers.my_app]
    image="myapp"

In the example above, "my_app" is an ID used at the configuration level, and `image` specifies the name of the image in the container registry.

You may want to define additional fields like `registry`, `namespace`, or `version` for more detailed configuration.
Here is an example of such advanced configuration:

.. code-block:: toml

    [default.containers.my_app]
    registry="quay.io"
    namespace="myproject"
    image="myapp"
    version="latest"

In the example above, we are specifying not just the image name, but also the container registry, namespace, and version.
This provides more control over the container configuration.

Include a Container from a Local Folder
---------------------------------------

Sometimes, you might need to develop your containers locally and include them in the image without using an external registry.
In such cases you can define a container in your configuration file as follows:

.. code-block:: toml

    [default.containers.my_app]
    image="myapp"
    version="latest"
    container_context="data/containers/my_app"

In this configuration, the `container_context` field defines the path where the container is defined.
During the build process, `autosd-demo` will build the containers and include them in the final image without using any external registry.

Additionally, you can specify the `container_file` field to indicate the name of the "`Containerfile`" when:

- The name of the file is not "`Containerfile`".
- The file is not at the root of the `container_context`.

For example, if your "`Containerfile`" is named "`Dockerfile`" and placed at "`data/containers/my_app/container`":

.. code-block:: toml

    [default.containers.my_app]
    image="myapp"
    version="latest"
    container_context="data/containers/my_app"
    container_file="container/Dockerfile"

Pass Build Arguments
--------------------

You can use docker build arguments to add flexibility to your builds.
See `docker guide <https://docs.docker.com/build/guide/build-args/>`_ for more information about the build arguments.
You can provide multiple build arguments to customize the resulting image.

For example, suppose your build argument is called "`FEDORA_VERSION`".
After adding its usage (preferably with the default value, i.e., "`ARG FEDORA_VERSION=39`") inside the "`Containerfile`", you can define it in your TOML file as follows:

.. code-block:: toml

    [default.containers.my_app.build_args]
    FEDORA_VERSION="40"

Modify a Container Quadlet File
----------------------------------

By default, a Quadlet file is generated to manage the containers as a `systemd` service unit.
The default file looks like this:

.. code-block:: systemd

    [Unit]
    Description=my_app container

    [Container]
    ContainerName=my_app
    Image=localhost/my_app:latest

    [Install]
    WantedBy=multi-user.target

    [Service]
    Restart=always

However, in many situations, you may need to modify this generated file to include or modify fields.
This could be to define a custom `exec` command, list volumes that should be added to the container,
specify devices to which the container should have access, or simply define `systemd` dependencies.

This is how to the default generated Quadlet configuration can be modified:

.. code-block:: toml

    [default.containers.my_app.systemd.unit]
    description="this is my container"
    requires="otherapp.socket"

    [default.containers.my_app.systemd.container]
    container_name="my_new_app"
    exec="/usr/bin/myapp -d"
    volume=[
        "/var:/var",
        "/home/user:/home/user"
    ]

As shown in the previous example, any field or section under `default.containers.my_app.systemd` will modify the Quadlet file generated for the "my_app" container.
The resulting Quadlet configuration would look like this:

.. code-block:: systemd

    [Unit]
    Description=this is my container
    Requires=otherapp.socket

    [Container]
    ContainerName=my_new_app
    Image=localhost/my_app:latest
    Exec=/usr/bin/myapp -d
    Volume=/var:/var
    Volume=/home/user:/home/user

    [Install]
    WantedBy=multi-user.target

    [Service]
    Restart=always

Furthermore, you can use this feature in combination with the `autosd-demo` environments to define a "`devel`" environment that changes the `exec` command in order to add a verbose flag.

.. code-block:: toml

    [default.containers.my_app.systemd.container]
    container_name="my_new_app"
    exec="/usr/bin/myapp -d"

    [devel.containers.my_app.systemd.container]
    exec="/usr/bin/myapp -d --verbose"

Disable Quadlet generation
--------------------------

In case you want the container image copied but do not need `systemd` to handle it,
you can explicitly disable Quadlet file creation:


.. code-block:: toml

    [default.containers.my_app]
    image="myapp"
    container_context="data/containers/my_app"
    quadlet=false

Registry Credentials
--------------------

Sometimes the containers are located in private container registries.
In these cases, you will need to provide credentials to allow the build process to download those containers.
To do this, you can add the following configuration where "quay_io" is an ID and `registry`, `username` & `password` are the credentials.

.. code-block:: toml

    [default.registry_creds.quay_io]
    registry="quay.io"
    username="myuser"
    password="my-secret-token"

Please note that if you do not want to include the password in the configuration file because the project needs to be pushed to a `Git` repository, you can store the password in the "`.secrets.toml`" file.
This process will be elaborated further in the upcoming `Secrets`_ section.

Manage `systemd` Services
=========================

You can enable / disable any `systemd` services using the `enabled_services` and `disabled_services` syntax.
For example, you can enable some useful monitoring services as follows:

.. code-block:: toml

    [default.systemd._monitoring]
    enabled_services=["redis.service", "pmproxy.service"]

RPM Packages and Repositories
=============================

The AutoSD images are built using `automotive-image-builder` and `osbuild` tools.
These tools construct the images using the RPM packages provided by the AutoSD distribution.

If you wish to build an image and include some RPM packages from the AutoSD repository or a custom repository, it is relatively straightforward.
You simply need to include the relevant configuration in your `autosd-demo` configuration file.

For including additional packages use `rpm.<ID>` syntax and define a list of packages that you would need to build the image:

.. code-block:: toml

    [default.rpm.extra_packages]
    packages=["vim", "openssh-server"]

Where "extra_packages" is an ID.

For specifying a custom repository use `rpm_repo.<ID>` syntax:

.. code-block:: toml

    [default.rpm_repo.my_copr_repo]
    baseurl="https://download.copr.fedorainfracloud.org/results/my_copr/my_packages/"

Where "my_copr_repo" is an ID.

This ensures that you can customize the RPM packages and repositories as per your product requirements.

.. note::
    In both cases, `rpm` or `rpm_repo`, you can add multiple sections at your convenience using different IDs.


Local Repositories
------------------

To include packages that have been built locally, you can designate a local repository in the configuration.
Here is how you can do it:

.. code-block:: toml

    [default.rpm_repo.my_custom_local_repo]
    baseurl="file:///home/myuser/rpmbuild/RPMS"

In this example, "`my_custom_local_repo`" is merely an identifier. The `baseurl` points to the local directory where your built RPM packages are located.

This way, you can include locally built packages in your project by adding a local repository to the configuration file.

.. note::
    You need to execute "`createrepo`" in the `RPMS` folder before the build starts so that the full directory structure is correctly created.

Files, Templated Files and Directories
=======================================

When you are constructing a demo or prototyping a new feature, it is not always about just adding containers or RPMs.
There are many cases where you need to include assets, regular files, or configuration files in your image.
I.e., to configure an installed package or share configuration/assets between containers.

To add files or templated files, you need to use the `extra_files.<ID>` syntax.
The files or the templated files will be allocated in a "`data/`" folder in your project.

Add single files
----------------

A possible structure of the directory "`data/`" could be:

.. code-block:: sh

    .
    ├── data
    │   ├── conf
    │   │   └── mydemo.conf.j2
    │   └── examples
    │       └── file.png

1. If you want to include the "`file.png`" in the image, the configuration would be as follows:

.. code-block:: toml

    [default.extra_files.my_demo_picture]
    src="data/examples/file.png"
    dest="/usr/local/share/examples/"

Where "my_demo_picture" is an ID.

2. If you want to include a templated file like "`data/conf/mydemo.conf.j2`", the configuration would be as follows:

.. code-block:: toml

    [default.extra_files.my_demo_config]
    src="data/conf/mydemo.conf.j2"
    dest="/etc/mydemo/mydemo.conf"

In this case, "my_demo_config" is an ID, and "`mydemo.conf.j2`" is a Jinja template.

The content of the "`mydemo.conf.j2`" could be something like:

.. code-block:: ini

    demo_version={{ conf.version }}

In the template, you have access to any configuration key in your project through the "`conf`" variable.
You could use all the capabilities of the Jinja templates to employ loops, conditionals, etc.

Add multiple files
------------------

A possible structure of the directory "`data/files/`" could be:

.. code-block:: sh

    .
    ├── data
    │   └── files
    │       └── {conf.name}
    │           ├── conf
    │           │   └── example.conf.j2
    │           └── images
    │               ├── img1.png
    │               ├── img2.png
    │               ├── img3.png
    │               ├── img4.png
    │               └── img5.png

To add multiple files and directories at once, you can configure it as follows:

.. code-block:: toml

    [default.extra_files.add_many_extra_files]
    src="data/files"
    dest="/usr/share/"

This configuration will copy the entire content of "`data/files/`" to "`/usr/share/`" in the image.

As you see, the "`{conf.name}`" is used to craft a path dynamically based on the configuration value.
This adds more flexibility when managing files and directories.

Add empty files and directories
-------------------------------

To create an empty file or directory, you need to refer the destination location only:

.. code-block:: toml

    [default.extra_files.add_empty_file]
    dest="/var/lib/flatpak/.changed"
    touch=true

"`touch=true`" instructs the builder to create an empty destination file.

To create a directory without content, put "`/`" at the end of the destination name:

.. code-block:: toml

    [default.extra_files.add_empty_directory]
    dest="/var/lib/misc/"
    touch=true

Modify permissions and ownership
--------------------------------

To change permissions and ownership for files & directories copied into the image, you need to use the `fs_perm.<ID>` syntax.
You are able to modify the permissions, `user` & `group` ownership, whether the change should be recursive, etc.

For example, to change recursively all ".txt" files' permissions, you can configure it as follows:

.. code-block:: toml

    [default.fs_perm.change_text_files_permissions]
    path="/etc/conf/*.txt"
    recursive=true
    user="anderson"
    group="anderson"
    perms="677"

.. note::
    1. Only the `path` is mandatory, any other option may be omitted.
    2. The `perms` expects a string and supports both octal mode and symbolic mode, as in the `chmod <https://linux.die.net/man/1/chmod>`_ command.

Download for link-type sources
------------------------------

If the `src` label in "`extra_files`" contains a link (i.e., "`https://`", "`ftp://`", etc.), download the file referenced by the link during the build and use it as defined.

Example:

.. code-block:: toml

    [default.extra_files.download_android_qcow]
    src="http://www.android.org/android.qcow2"
    dest="/var/lib/android"
    checksum="sha1:14091823098102380198409184098243"

The downloaded files are saved in "`<base_dir>/cache/downloads`" directory to be used in subsequent builds.
If "`checksum`" label is defined, the cached file checksum is checked and the file is re-downloaded in case the checksums differ.


Dracut configuration
====================

If your demo requires some additional dracut configuration like adding modules or install files you can include the modules or files in this way.

.. code-block:: toml

    [default.dracut.my_app]
    modules=["my_module"]
    install=["/etc/my_app/dracut.file"]


MPP variables
=============

There is a number of osbuild-mpp variables that are implicitly handled
by `autosd-demo`, including dracut variables, and extra repositories and rpms,
among others:

- `use_containers_extra_store`
- `use_qm_containers_extra_store`
- `use_qm`
- `use_bluechi`

If your demo requires additional osbuild-mpp variables not listed above,
`autosd-demo` allows you to add these variables to the project configuration.

.. code-block:: toml

    [default.mpp_vars]
    use_composefs=true


The QM Layer
============

AutoSD provides an environment to run Quality Managed (QM) containers.
This dedicated environment within the operating system is a specific container, referred to as the "`qm`" container.
This special container is structured with its own filesystem, its own `systemd`, and its own containers.
This arrangement offers freedom from interference and isolation from the rest of the system and workloads.

Throughout the previous sections, we have covered how you can install containers, RPMS, RPM repositories, files and templated files using the `autosd-demo` configuration.
If you wish to accommodate these types of assets within the "`qm`" layer, it is as simple as adding the field `layer` with the value of "`qm`" to the configuration.

Here are a few examples of configuration for the "`qm`" layer:

.. code-block:: toml

    [default.containers.qm_my_app]
    image="myapp"
    version="latest"
    container_context="data/containers/my_app"
    layer="qm"

.. code-block:: toml

    [default.rpm.qm_extra_packages]
    packages=["vim", "openssh-server"]
    layer="qm"

    [default.rpm_repo.qm_my_copr_repo]
    baseurl="https://download.copr.fedorainfracloud.org/results/my_copr/my_packages/"
    layer="qm"

.. code-block:: toml

    [default.extra_files.qm_add_many_extra_files]
    src="data/files"
    dest="/usr/share/"
    layer="qm"

.. code-block:: toml

    [default.fs_perm.qm_hide_my_precious]
    path="/usr/share/my_precious"
    user="neo"
    group="neo"
    perms="o-rwx"
    layer="qm"

In these examples, you can see how assets are assigned to the "`qm`" layer by adding `layer="qm"` to their configuration in the TOML file.

Hooks
=====

Your demo may require to execute additional steps such as:

- Install additional packages because your demo requires it
- Execute some tasks before and after the build process
- Clean some files once the build process finished

The `autosd-demo` tool allows the user to extend and include these additional steps through Ansible tasks files.
So you could write a "setup.yaml" file in your demo folder as follows:

.. code-block:: yaml

    - name: Ensure vim is present
      ansible.builtin.package:
        name: "vim"
        state: present
      become: True

and then enable the hook in your `.autosd-demo.toml` file

.. code-block:: toml

    [default.hooks.my_demo]
    setup=["setup.yaml"]

The `hooks` section allows different stages like: `setup`, `pre_build`, `post_build`, `clean_build` so you could define more complex examples like this.

.. code-block:: toml

    [default.hooks.my_demo]
    setup=["install_pkgs.yaml"]
    pre_build=["transform_some_files.yaml", "copy_files_from_remote_sever.yaml"]
    post_build=["upload_images.yaml"]
    clean_build=["clean_files.yaml"]

Addons
======

The `autosd-demo` tool includes an addon feature that allows you to enrich your demo by enabling specific functionalities in the form of containers, RPMs, files, or other artifacts.

The existence of several common features has led to the creation of these addons.
They include:

- `ssh_server`: Incorporates an SSH server into your image.

- `shell_extras`: This includes enhanced shell utilities, debugging tools, and vim into your image for added functionality and convenience during debugging operations.

- `podman_next`: Integrates the latest version of podman into your image.

- `monitoring`: Installs and configures the Performance Co-Pilot (PCP) agent into your demo's image, providing monitoring capabilities.

To get the complete list of available addons, run:

.. code-block:: bash

    $ autosd-demo addons list
    can_utils
    monitoring
    mutter
    <...more lines of output...>

For every addon, there is a description that can be viewed:

.. code-block:: bash

    $ autosd-demo addons show mutter

    mutter
    ------

    Installs Mutter to be used with Wayland.

You can quickly enable these addons in your `autosd-demo` configuration by including them in the `addons` array as follows:

.. code-block:: toml

    [default]
    addons=["ssh_server", "shell_extras", "monitoring"]

In this manner, you can customize your demo setup and efficiently reuse these addons across different demos.

Build options configuration
===========================

If for your demo it's clear what build options should be included when you execute `autosd-demo build` you can define any of those options as follows:

.. code-block:: toml

    [default.build]
    distro="fedora"
    target="qemu"
    image_mode="package"
    image_arch="x86_64"
    export="tar"
    ostree_repo="@jinja {{ conf.base_dir }}/ostree_repo"
    defines=['distro_version="1"']

Take into account that any setting defined in the configuration takes precedence to configuration coming from the command line options, which will be ignored. The only exception to that being `--define` options, where additional defines can be added through the command line option.

.. _remote-building:

Remote Building
===============

Building demos remotely is a common requirement.
Especially when you are developing on an `x86_64` laptop and need to test the image on an `arm64` board, or if you wish to employ the resources of a powerful remote server for building.

`autosd-demo` supports executing most of its operations remotely, taking advantage of Ansible, which it is built upon.
This includes tasks such as building containers, templating files, and other functionalities.

Setting up a remote server for building involves using a Fedora or CentOS instance with an SSH server active.
Once these prerequisites are met, you can include the following settings (typically in the `Global Configuration`_ file):

.. code-block:: toml

    [default.remote_host.my_builder]
    user="myuser"
    address="192.168.1.23"

After this configuration is in place, you can ensure the remote server is prepared to install the required software.
This can be done using the following command:

.. code-block:: bash

    $ autosd-demo setup -H my_builder

You can build the project remotely with this command:

.. code-block:: bash

    $ autosd-demo build -H my_builder

The build process occurs remotely and includes tasks such as custom container compilation, templating files, and executing the `automotive-image-builder` to produce the final image.
Upon completion, `autosd-demo` downloads the created image to your local environment for further use or testing.

Composing and Emulating External Services
=========================================

While preparing a demo, it is common to require external services that emulate various interactions.
For example, you might need a fully configured Grafana instance to observe metrics from the `monitoring` addon, a container registry to demonstrate updates, or external APIs providing information for a digital cockpit application demo.

To facilitate the emulation and configuration of these kinds of services, `autosd-demo` offers a `compose` command, which is essentially a wrapper around `podman-compose`. This command allows users to define and use composite services.

If you wish to use preconfigured composite services provided by `autosd-demo`, you just need to add them to your configuration file:

.. code-block:: toml

    [default.compose]
    enable=["monitoring"]

For using your own services, include them in a "`compose.yaml`" file in the root directory of your demo folder.

When the external services are required, they can be spinned up by executing:

.. code-block:: bash

    $ autosd-demo compose up

And can be brought down when not needed using:

.. code-block:: bash

    $ autosd-demo compose down

Therefore, `autosd-demo` provides flexibility in managing external services, allowing users to emulate intricate real-world scenarios for their demonstrations.

Exporting a Demo
================

`autosd-demo` serves as a valuable tool for creating, prototyping, and building demos.
It becomes especially powerful when used in conjunction with tools like `automotive-image-builder` and various external services.
In particular, the `automotive-image-builder` operates as the official builder.

There may be situations where you begin using `autosd-demo` to prototype and share an initial version of the demo you are working on.
Once you are ready to transition from the prototyping phase, `autosd-demo` enables you to export your demo.
This will generate an `osbuild` manifest for `automotive-image-builder` to use.

It is important to note that certain features, such as custom container builds and external services composition, are not included in the export process.
These actions can be performed within `autosd-demo`, but will not be incorporated into the generated `osbuild` manifest.

When you wish to export your demo, execute this command:

.. code-block:: bash

    $ autosd-demo export

This command will transform your demo, with all its configurations, addons, and settings into an `osbuild` manifest.
Then, `automotive-image-builder` can use this manifest to construct a new image, effectively taking your demo to the next stage.
However, any custom container builds or composed external services should be managed separately, as they will not be part of the exported manifest.

Secrets
=======

In order to store sensitive information, avoiding leaking that information in the project configuration, `autosd-demo` supports secrets.

To that end, you can write all your sensitive data in the "`.secrets.toml`" file and place it in the project root.

For example, you can have the following configuration in your project:

.. code-block:: toml

    [default.registry_creds.quay_io]
    registry="quay.io"
    username="myuser"

And have the password stored in the secrets file:

.. code-block:: toml

    [default.registry_creds.quay_io]
    password="my-password"

Secrets definition is integrated with your project configuration, including environments.
This way you can store as many secrets as you need and change them depending on the current environment.

Configuration Interpolation
===========================

The `autosd-demo` configuration supports template substitution.
To do so, use the `@format` token in the value of a configuration field.
Then, in the string you can refer to environment variables via `env`, and variables defined in the configuration via `this`.

For example:

.. code-block:: toml

    [default]
    name="my_app"
    version="0.0.1"

    [default.containers.my_app]
    registry="quay.io"
    namespace="my_demo"
    container_context="@format {env[HOME]}/data/containers/my_app"
    version="@format {this.version}"

See `Dynaconf <https://www.dynaconf.com/dynamic/>`_ for a complete list of possible formats.
