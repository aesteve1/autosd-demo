import os
from pathlib import Path

import pytest

from autosd_demo import AUTOSD_DEMO_LOCAL_CONF_FILENAME


@pytest.mark.integration
def test_cli_new(empty_ws: Path):
    assert os.system("autosd-demo new test") == 0
    assert empty_ws.joinpath("test", AUTOSD_DEMO_LOCAL_CONF_FILENAME).exists()
