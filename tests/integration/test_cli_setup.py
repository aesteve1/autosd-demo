import os
from pathlib import Path

import pytest
import yaml

from autosd_demo import AUTOSD_DEMO_DATA_PATH, AUTOSD_DEMO_LOCAL_CONF_FILENAME


@pytest.mark.integration
def test_cli_setup(minimal_ws: Path):
    assert minimal_ws.joinpath(AUTOSD_DEMO_LOCAL_CONF_FILENAME).exists()
    assert os.system("autosd-demo setup") == 0

    ansible_defaults_path = Path(
        AUTOSD_DEMO_DATA_PATH, "ansible", "vars", "default.yaml"
    )
    with ansible_defaults_path.open() as fd:
        ansible_defaults = yaml.safe_load(fd)

    for pkg in ansible_defaults["required_pkgs"]:
        assert os.system(f"rpm -qi {pkg} > /dev/null") == 0
