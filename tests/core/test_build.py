from pathlib import Path
from unittest.mock import patch

import pytest

from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.core.build import BuildEnv, reduce_directory_list
from autosd_demo.core.osbuild import OSBuildConfig


class TestBuildEnv(BuildEnv):
    def __init__(self, osbuild=False):
        super(BuildEnv, self).__init__()

        self.update(
            {
                "build": {
                    "src": {
                        "directories": [],
                        "files": [],
                        "templated_files": [],
                        "fetch_files": [],
                    }
                }
            }
        )

        if osbuild:
            self.update(
                {"osbuild": OSBuildConfig(build_files=[], build_directories=[])}
            )


@pytest.mark.parametrize(
    "settings_data, expected",
    [
        (
            {
                "remote_host": {
                    "foo": {"base_dir": "/home/foo", "build_dir": "/home/foo/build"}
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        (
            {
                "remote_host": {
                    "foo": {
                        "base_dir": "/home/foo",
                    }
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        ({"remote_host": {"foo": {}}}, {"base_dir": "~", "build_dir": "~/build"}),
        ({}, {"base_dir": "~", "build_dir": "~/build"}),
    ],
)
@patch("autosd_demo.utils.build.settings")
@patch("autosd_demo.core.build.in_autosd_demo", return_value=True)
def test_get_build_env_config_with_remote_env(
    _, mock_settings, settings_data, expected
):
    mock_settings.as_dict.return_value = settings_data
    result = BuildEnv(host="foo")
    assert "conf" in result
    assert "build" in result
    assert result["build"]["base_dir"] == expected["base_dir"]
    assert result["build"]["build_dir"] == expected["build_dir"]


@patch("autosd_demo.utils.build.settings")
@patch("autosd_demo.core.build.in_autosd_demo", return_value=False)
def test_get_build_env_config_with_no_env(_, mock_settings):
    mock_settings.as_dict.return_value = {}
    build_env = BuildEnv(host=None)
    assert "base_dir" not in build_env["build"]
    assert "build_dir" not in build_env["build"]
    assert build_env["build"]["is_remote"] is False


@patch("autosd_demo.utils.build.settings")
@patch("autosd_demo.core.build.in_autosd_demo", return_value=True)
def test_get_build_env_config_with_local_env(_, mock_settings):
    mock_settings.as_dict.return_value = {
        "base_dir": "~/src",
        "build_dir": "~/src/build",
    }
    build_env = BuildEnv(host=None)
    assert build_env["conf"]["base_dir"] == "~/src"
    assert build_env["conf"]["build_dir"] == "~/src/build"
    assert build_env["build"]["base_dir"] == "~/src"
    assert build_env["build"]["build_dir"] == "~/src/build"
    assert build_env["build"]["is_remote"] is False


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        (
            {},
            {
                "directories": set(),
                "templated_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "centos_sig": {
                    "autosd_demo": {
                        "template_systemd_container_path": "/test/systemd.j2"
                    }
                },
                "containers": {"alpha": {}, "beta": {"layer": "qm"}},
            },
            {
                "directories": {
                    "files/mydemo/extra_files/qm_fs/etc/containers/systemd",
                    "files/mydemo/extra_files/root_fs/etc/containers/systemd",
                },
                "templated_files": [
                    {
                        "container_name": "alpha",
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "etc/containers/systemd/alpha.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                    {
                        "container_name": "beta",
                        "dest": (
                            "files/mydemo/extra_files/qm_fs/"
                            "etc/containers/systemd/beta.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "centos_sig": {
                    "autosd_demo": {
                        "template_systemd_container_path": "/test/systemd.j2"
                    }
                },
                "containers": {"alpha": {"quadlet": False}, "beta": {}},
            },
            {
                "directories": {
                    "files/mydemo/extra_files/root_fs/etc/containers/systemd",
                },
                "templated_files": [
                    {
                        "container_name": "beta",
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "etc/containers/systemd/beta.container"
                        ),
                        "src": "/test/systemd.j2",
                    }
                ],
            },
        ),
    ],
)
def test_get_build_src_quadlets(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": set(),
        "templated_files": [],
    }
    test._get_build_src_quadlets(src_contents)
    assert src_contents == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, extra_files, expected_result",
    [
        (
            {},
            [],
            {
                "directories": set(),
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "containers": {"alpha": {"container_context": "containers/alpha"}},
            },
            [
                "containers/alpha/Containerfile",
                "containers/alpha/app.py",
                "containers/alpha/conf/config.yaml.j2",
            ],
            {
                "directories": {
                    "files/mydemo/containers/alpha",
                    "files/mydemo/containers/alpha/conf",
                },
                "files": [
                    {
                        "dest": "files/mydemo/containers/alpha/Containerfile",
                        "src": "containers/alpha/Containerfile",
                    },
                    {
                        "dest": "files/mydemo/containers/alpha/app.py",
                        "src": "containers/alpha/app.py",
                    },
                ],
                "templated_files": [
                    {
                        "dest": "files/mydemo/containers/alpha/conf/config.yaml",
                        "src": "containers/alpha/conf/config.yaml.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_custom_containers_files(
    mock_settings, conf, extra_files, expected_result, tmp_dir
):
    def sort_container_files(data):
        return {
            key: sorted(value, key=lambda d: d["src"] if isinstance(d, dict) else d)
            for key, value in sorted(data.items())
        }

    conf.update({"base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": set(),
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }

    for extra_file in extra_files:
        extra_file_path = tmp_dir / Path(extra_file)
        extra_file_path.parent.mkdir(parents=True, exist_ok=True)
        extra_file_path.touch()

    test._get_build_src_custom_containers_files(src_contents)
    assert sort_container_files(src_contents) == sort_container_files(expected_result)


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf",
    [
        (
            {
                "name": "mydemo",
                "extra_files": {"test": {"src": "data/assets"}},
            }
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {"test": {"dest": "usr/share/demo"}},
            }
        ),
    ],
)
def test_get_build_src_extra_files_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": set(),
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }

    with pytest.raises(ValidationError):
        test._get_build_src_extra_files(src_contents)


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, extra_files, expected_result",
    [
        (
            {},
            [],
            {
                "directories": set(),
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            [
                "data/assets/text1.txt",
                "data/assets/text2.txt.j2",
            ],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    }
                ],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text2.txt"
                        ),
                        "src": "data/assets/text2.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                    },
                    "test2": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/text1.txt",
                        "layer": "qm",
                    },
                },
            },
            ["data/assets/text1.txt"],
            {
                "directories": {
                    "files/mydemo/extra_files/qm_fs/usr/share",
                    "files/mydemo/extra_files/qm_fs/usr/share/demo",
                },
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/qm_fs/usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    },
                    {
                        "dest": "files/mydemo/extra_files/qm_fs/usr/share/text1.txt",
                        "src": "data/assets/text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets/text1.txt", "dest": "usr/share/demo/"}
                },
            },
            [],
            {
                "directories": set(),
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/test.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            ["data/assets/subfolder/text1.txt.j2"],
            {
                "directories": {
                    "files/mydemo/extra_files/root_fs/usr/share/demo/subfolder"
                },
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/subfolder/text1.txt"
                        ),
                        "src": "data/assets/subfolder/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            [],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/usr/share/demo/test.txt"
                        ),
                        "src": "cache/downloads/http_remote.org_data_text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "",
                        "file": "http_remote.org_data_text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "dest": "usr/share/demo/",
                    }
                },
            },
            [],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/usr/share/demo/text1.txt"
                        ),
                        "src": "cache/downloads/http_remote.org_data_text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "",
                        "file": "http_remote.org_data_text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "checksum": "sha1:9e9c26a6cfee814567796d6c3c1add61d135ce73",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            [],
            {
                "directories": {"files/mydemo/extra_files/root_fs/usr/share/demo"},
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/usr/share/demo/test.txt"
                        ),
                        "src": "cache/downloads/http_remote.org_data_text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "sha1:9e9c26a6cfee814567796d6c3c1add61d135ce73",
                        "file": "http_remote.org_data_text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                        "touch": "true",
                    },
                    "test2": {
                        "dest": "usr/share/text1.txt",
                        "touch": "true",
                    },
                },
            },
            [],
            {
                "directories": {
                    "files/mydemo/extra_files/root_fs/usr/share",
                    "files/mydemo/extra_files/qm_fs/usr/share/demo",
                },
                "files": [
                    {
                        "dest": "files/mydemo/extra_files/root_fs/usr/share/text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_extra_files(
    mock_settings, conf, extra_files, expected_result, tmp_dir
):
    conf.update({"base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": set(),
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }

    for extra_file in extra_files:
        extra_file_path = tmp_dir / Path(extra_file)
        extra_file_path.parent.mkdir(parents=True, exist_ok=True)
        extra_file_path.touch()

    test._get_build_src_extra_files(src_contents)
    assert src_contents == expected_result


@pytest.mark.parametrize(
    "directory_list, expected_result",
    [
        (
            ["/usr/local", "/usr/local/share", "/usr/local/share/demo"],
            ["/usr/local/share/demo"],
        ),
        (
            ["/usr/local", "/usr/local/share/demo", "/usr/share/demo"],
            ["/usr/local/share/demo", "/usr/share/demo"],
        ),
        (
            {"/usr/local", "/usr/local/share/demo", "/usr/share/demo"},
            ["/usr/local/share/demo", "/usr/share/demo"],
        ),
    ],
)
def test_reduce_directory_list(directory_list, expected_result):
    result = reduce_directory_list(directory_list)
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        (
            {},
            {},
        ),
        (
            {
                "containers": {"ffi_client": {"layer": "qm", "version": "latest"}},
            },
            {},
        ),
        (
            {
                "containers": {
                    "ffi_client": {
                        "layer": "qm",
                        "build_args": {
                            "FEDORA_VERSION": "38",
                            "TV_VERSION": "1.0",
                            "CALCULATOR_VERSION": "10",
                        },
                        "version": "latest",
                    },
                    "ffi_server": {
                        "build_args": {
                            "CAR_VERSION": "20",
                            "FEDORA_VERSION1": "38",
                            "FEDORA_VERSION": "40",
                            "FEDORA_VERSION2": "39",
                            "TV_VERSION": "2.0",
                        },
                        "version": "latest",
                    },
                },
            },
            {
                "ffi_client": [
                    "--build-arg FEDORA_VERSION=38",
                    "--build-arg TV_VERSION=1.0",
                    "--build-arg CALCULATOR_VERSION=10",
                ],
                "ffi_server": [
                    "--build-arg CAR_VERSION=20",
                    "--build-arg FEDORA_VERSION1=38",
                    "--build-arg FEDORA_VERSION=40",
                    "--build-arg FEDORA_VERSION2=39",
                    "--build-arg TV_VERSION=2.0",
                ],
            },
        ),
    ],
)
def test_get_podman_extra_args(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_podman_extra_args()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({}, {"clean_build": [], "post_build": [], "pre_build": [], "setup": []}),
        (
            {"hooks": {"my_demo": {"fake_stage": []}}},
            {"clean_build": [], "post_build": [], "pre_build": [], "setup": []},
        ),
        (
            {
                "hooks": {
                    "my_demo": {
                        "setup": ["setup.yaml"],
                        "pre_build": ["pre_build.yaml"],
                        "post_build": ["post_build.yaml"],
                        "clean_build": ["clean.yaml"],
                    }
                }
            },
            {
                "clean_build": ["/home/user/src/demo/clean.yaml"],
                "post_build": ["/home/user/src/demo/post_build.yaml"],
                "pre_build": ["/home/user/src/demo/pre_build.yaml"],
                "setup": ["/home/user/src/demo/setup.yaml"],
            },
        ),
        (
            {
                "hooks": {
                    "my_demo": {
                        "setup": ["/home/user/setup.yaml"],
                        "pre_build": ["/home/user/pre_build.yaml"],
                        "post_build": ["/home/user/post_build.yaml"],
                        "clean_build": ["/home/user/clean.yaml"],
                    }
                }
            },
            {
                "clean_build": ["/home/user/clean.yaml"],
                "post_build": ["/home/user/post_build.yaml"],
                "pre_build": ["/home/user/pre_build.yaml"],
                "setup": ["/home/user/setup.yaml"],
            },
        ),
    ],
)
def test_get_configured_hooks(mock_settings, conf, expected):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    test.host = False
    test.base_dir = "/home/user/src/demo"

    result = test._get_configured_hooks()

    assert result == expected


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({"containers": {"alpha": {"image": "fedora"}}}, []),
        (
            {"containers": {"alpha": {"container_context": "containers/alpha"}}},
            ["alpha"],
        ),
        (
            {
                "containers": {
                    "alpha": {"container_file": "containers/alpha/Containerfile"},
                    "beta": {"container_context": "containers/beta"},
                }
            },
            ["alpha", "beta"],
        ),
    ],
)
def test_get_build_custom_containers_ids(mock_settings, conf, expected):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()

    result = test._get_build_custom_containers_ids()

    assert set(result) == set(expected)


@patch("src.autosd_demo.utils.build.settings")
def test_get_preprocessed_osbuild_config(mock_settings):
    mock_settings.as_dict.return_value = {}
    expected_osbuild_conf = {
        "build": {
            "src": {
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            }
        },
        "osbuild": {
            "chmod": {},
            "chown": {},
            "containers": [],
            "has_qm": False,
            "extra_contents": {},
            "groups": {},
            "mpp_vars": {},
            "qm_chmod": {},
            "qm_chown": {},
            "qm_containers": [],
            "qm_extra_contents": {},
            "qm_groups": {},
            "qm_systemd": {},
            "qm_users": {},
            "systemd": {},
            "users": {},
        },
    }

    actual_osbuild_conf = TestBuildEnv(osbuild=True)

    assert actual_osbuild_conf == expected_osbuild_conf
    assert actual_osbuild_conf.build_files == []
    assert actual_osbuild_conf.build_directories == []
