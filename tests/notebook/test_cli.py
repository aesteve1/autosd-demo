from io import StringIO
from unittest.mock import patch

from autosd_demo.notebook import NotebookCLI


def test_notebook_cli():
    cli = NotebookCLI()
    assert hasattr(cli, "version")
    assert "version" in dir(cli)
    assert getattr(cli, "fake_cli") is None


def test_notebook_cli_show_hidden():
    cli = NotebookCLI(show_hidden=True)
    assert hasattr(cli, "ansible")
    assert "ansible" in dir(cli)


@patch("sys.stdout", new_callable=StringIO)
@patch("sys.stderr", new_callable=StringIO)
def test_notebook_cli_invoke(mock_stdout, mock_stderr):
    cli = NotebookCLI()
    assert isinstance(mock_stdout, StringIO)
    assert isinstance(mock_stderr, StringIO)
    assert cli.version() == 0
    assert cli.version("--help") == 0
    assert cli.version(["--help"]) == 0
    assert cli.version({}) == 1
