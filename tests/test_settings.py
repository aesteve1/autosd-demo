from pathlib import Path
from unittest.mock import patch

import pytest
from dynaconf.utils import DynaconfDict

from autosd_demo import AUTOSD_DEMO_LOCAL_CONF_FILENAME
from autosd_demo.settings import Settings

GLOBAL_SETTINGS = """
    [default]
    version="1.0.0"
"""


def test_settings(tmp_dir, global_config):
    settings_path = tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME
    settings_path.touch()

    global_config.write_text(GLOBAL_SETTINGS)
    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        settings = Settings()
    assert Path(settings.base_dir) == tmp_dir
    assert settings.version == "1.0.0"


enrich_container_settings_test_cases = [
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "unit",
        "description",
        "test_container container",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "container",
        "container_name",
        "test_container",
    ),
    (
        {"containers": {"test_container": {"version": "0.1"}}},
        "test_container",
        "container",
        "image",
        "localhost/test_container:0.1",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "install",
        "wanted_by",
        "multi-user.target",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {"service": {}}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {
            "containers": {
                "test_container": {"systemd": {"service": {"restart": "never"}}}
            }
        },
        "test_container",
        "service",
        "restart",
        "never",
    ),
]


@pytest.mark.parametrize(
    "settings,container_name,section,key,expected", enrich_container_settings_test_cases
)
def test_enrich_container_settings(settings, container_name, section, key, expected):
    mock_settings = DynaconfDict(settings)
    Settings._enrich_container_settings(mock_settings)
    assert (
        mock_settings["containers"][container_name]["systemd"][section][key] == expected
    )


@pytest.mark.parametrize(
    "settings,container_name,section,key",
    [
        (
            {
                "containers": {
                    "test_container": {"systemd": {"install": {"wanted_by": ""}}}
                }
            },
            "test_container",
            "install",
            "wanted_by",
        ),
        (
            {
                "containers": {
                    "test_container": {"systemd": {"service": {"restart": ""}}}
                }
            },
            "test_container",
            "service",
            "restart",
        ),
    ],
)
def test_enrich_container_settings_clean(settings, container_name, section, key):
    mock_settings = DynaconfDict(settings)
    Settings._enrich_container_settings(mock_settings)
    with pytest.raises(KeyError):
        mock_settings["containers"][container_name]["systemd"][section][key]


def test_get_addons_preload_includes(tmp_dir):
    with (tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME).open("w") as fd:
        fd.write("[default]\n")
        fd.write("addons=['weston', 'ssh_server', 'test']\n")

    settings = Settings()
    assert settings.rpm._ssh_server_pkgs.packages == ["openssh-server"]
    # Ensure nested addons are listed.
    assert "qm_dbus_broker" in settings.addons
    assert "wayland_session" in settings.addons
