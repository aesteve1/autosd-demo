from unittest.mock import MagicMock, patch

import pytest
from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.cli.exceptions import AnsibleError
from autosd_demo.core import Playbooks

HOST_PARAMETERS = [(None, False), ("foobar", True)]


@pytest.mark.parametrize("host,is_remote", HOST_PARAMETERS)
@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.cli.cmd_compose.Compose")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_build(
    _, mock_compose, mock_executor, mock_build_env, host, is_remote
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_run = MagicMock()
    mock_compose_build = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run = mock_executor_run
    mock_compose.return_value.build = mock_compose_build
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = is_remote

    runner = CliRunner()
    cmd = ["compose", "build"]
    if is_remote:
        cmd.extend(["-H", host])
    result = runner.invoke(cli, cmd)
    assert result.exit_code == 0

    mock_build_env.assert_called_once_with(host)
    mock_executor.assert_called_once_with(remote=is_remote, become_required=False)
    mock_executor_ctx.assert_called_once_with(host=host)
    mock_executor_run.assert_called_once()
    mock_compose.assert_called_once()
    mock_compose_build.assert_called_once()


@pytest.mark.parametrize("host,is_remote", HOST_PARAMETERS)
@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.cli.cmd_compose.Compose")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_up(
    _, mock_compose, mock_executor, mock_build_env, host, is_remote
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_run = MagicMock()
    mock_compose_up = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run = mock_executor_run
    mock_compose.return_value.up = mock_compose_up
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = is_remote

    runner = CliRunner()
    cmd = ["compose", "up"]
    if is_remote:
        cmd.extend(["-H", host])
    result = runner.invoke(cli, cmd)
    assert result.exit_code == 0

    mock_build_env.assert_called_once_with(host)
    mock_executor.assert_called_once_with(remote=is_remote, become_required=False)
    mock_executor_ctx.assert_called_once_with(host=host)
    mock_executor_run.assert_called_once()
    mock_compose.assert_called_once()
    mock_compose_up.assert_called_once()


@pytest.mark.parametrize("host,is_remote", HOST_PARAMETERS)
@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.cli.cmd_compose.Compose")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_down(
    _, mock_compose, mock_executor, mock_build_env, host, is_remote
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_run = MagicMock()
    mock_compose_down = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run = mock_executor_run
    mock_compose.return_value.down = mock_compose_down
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = is_remote

    runner = CliRunner()
    cmd = ["compose", "down"]
    if is_remote:
        cmd.extend(["-H", host])
    result = runner.invoke(cli, cmd)
    assert result.exit_code == 0

    mock_build_env.assert_called_once_with(host)
    mock_executor.assert_called_once_with(remote=is_remote, become_required=False)
    mock_executor_ctx.assert_called_once_with(host=host)
    mock_executor_run.assert_called_once()
    mock_compose.assert_called_once()
    mock_compose_down.assert_called_once()


@pytest.mark.parametrize("host,is_remote", HOST_PARAMETERS)
@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.cli.cmd_compose.Compose")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_config(
    _, mock_compose, mock_executor, mock_build_env, host, is_remote
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_run = MagicMock()
    mock_compose_config = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run = mock_executor_run
    mock_compose.return_value.config = mock_compose_config
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = is_remote

    runner = CliRunner()
    cmd = ["compose", "config"]
    if is_remote:
        cmd.extend(["-H", host])
    result = runner.invoke(cli, cmd)
    assert result.exit_code == 0

    mock_build_env.assert_called_once_with(host)
    mock_executor.assert_called_once_with(remote=is_remote, become_required=False)
    mock_executor_ctx.assert_called_once_with(host=host)
    mock_executor_run.assert_called_once()
    mock_compose.assert_called_once()
    mock_compose_config.assert_called_once()


@pytest.mark.parametrize("cmd", ("up", "down", "build", "config"))
@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_error(_, mock_executor, mock_build_env, cmd):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = AnsibleError(1, Playbooks.PREPARE_COMPOSE)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    run_cmd = ["compose", cmd]
    result = runner.invoke(cli, run_cmd)

    mock_build_env.assert_called_once_with(None)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.PREPARE_COMPOSE, extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 1
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output
