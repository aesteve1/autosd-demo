from pathlib import Path
from unittest.mock import mock_open, patch

from click.testing import CliRunner

from autosd_demo.cli import cli

ADDON_NAME = "some_addon"
ADDON_PATH = "share/autosd-demo/addons/" + ADDON_NAME
ADDONS_METADATA = {ADDON_NAME: {"base_dir": ADDON_PATH}}

ADDON_MODULE = '''"""
A long description.
"""
'''

LIST_OUTPUT = f"{ADDON_NAME}\n"

SHOW_OUTPUT = f"""
{ADDON_NAME}
----------

A long description.

"""

SHOW_ERROR_OUTPUT = f"""
{ADDON_NAME}
----------

No description found.

"""


@patch("autosd_demo.cli.cmd_addons.get_addons_metadata", return_value=ADDONS_METADATA)
def test_cmd_addons_list(_):
    runner = CliRunner()
    result = runner.invoke(cli, ["addons", "list"])
    assert result.exit_code == 0
    assert result.output == LIST_OUTPUT


@patch("io.open", new_callable=mock_open, read_data=ADDON_MODULE)
@patch("autosd_demo.cli.cmd_addons.get_addons_metadata", return_value=ADDONS_METADATA)
def test_cmd_addons_show(_, io_open):
    runner = CliRunner()
    result = runner.invoke(cli, ["addons", "show", "some_addon"])
    assert result.exit_code == 0
    io_open.assert_called_with(
        Path(ADDON_PATH) / "__init__.py", "r", -1, "locale", None, None
    )
    assert result.output == SHOW_OUTPUT


@patch("io.open", new_callable=mock_open, read_data=ADDON_MODULE)
@patch("autosd_demo.cli.cmd_addons.get_addons_metadata", return_value=ADDONS_METADATA)
def test_cmd_addons_show_unknown_addon(_, io_open):
    runner = CliRunner()
    result = runner.invoke(cli, ["addons", "show", "some_other_addon"])
    assert result.exit_code != 0
    io_open.assert_not_called()


@patch("io.open", side_effect=FileNotFoundError)
@patch("autosd_demo.cli.cmd_addons.get_addons_metadata", return_value=ADDONS_METADATA)
def test_cmd_addons_missing_description(_, io_open):
    runner = CliRunner()
    result = runner.invoke(cli, ["addons", "show", "some_addon"])
    assert result.exit_code == 0
    io_open.assert_called_with(
        Path(ADDON_PATH) / "__init__.py", "r", -1, "locale", None, None
    )
    assert result.output == SHOW_ERROR_OUTPUT
