from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.cli.exceptions import AnsibleError
from autosd_demo.core import Playbooks


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=False)
def test_build_no_base_dir(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_build_env.assert_not_called()


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 6


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_enable_pw(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = AnsibleError(1, Playbooks.DISABLE_SUDO_PASSWD)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 1
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_prepare_env(_, mock_executor, mock_build_env):
    side_effects = [None] * 5
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    # Cleanup if prepare env fails
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 5
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_prepare_env_no_clean(_, mock_executor, mock_build_env):
    side_effects = [None] * 4
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    # Do not clean, but still disable nopassword when fail
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 4
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_build_post_env_no_clean(_, mock_executor, mock_build_env):
    side_effects = [None] * 5
    side_effects[3] = AnsibleError(1, Playbooks.POST_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD, extra_vars=mock_build_env.return_value
    )
    # Do not clean, but still disable nopassword when fail
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 5
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_setup_with_remote_env(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = True

    runner = CliRunner()
    runner.invoke(cli, ["build", "-H", "foobar"])

    mock_executor.assert_called_once_with(remote=True, become_required=True)
    mock_executor_ctx.assert_called_once_with(host="foobar")
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 6


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env_and_no_clean(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        Playbooks.DISABLE_SUDO_PASSWD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD, extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env_and_verbose(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run = MagicMock(return_value=0)
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run_builder = mock_run
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--verbose"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run.assert_called_once_with(AnyStringWith("--verbose"))
    assert mock_run.call_count == 1
