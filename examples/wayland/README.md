# Wayland

This example project demonstrates a [Wayland](https://wayland.freedesktop.org/)
compositor running in AutoSD, in the QM container.

The example showcases two environments for [Weston](https://gitlab.freedesktop.org/wayland/weston)
and [Mutter](https://mutter.gnome.org/), respectively. In both cases, it runs
a demo weston-terminal on top.

## Addons

This example features some addons that are part of both `weston` and `wayland`
addons, so it does not need to be directly imported in the main project
configuration file for these compositors.

As they are hidden behind a nested addon import, we will detail them in this
Section. Take into account that even though they are currently hidden, they
can still be directly instantiated for other projects if needed.

### QM DBus Broker

This addon creates a new systemd socket at `/run/dbus/qm_bus_socket`, which
then is handled to a containerized service running a user-scoped dbus-broker
instance to handle accesses to the bus.

The `qm_bus_socket` is meant to be used by applications running inside the QM
container that do not require accessing the system bus socket. This way
we avoid flooding the main system socket, which could potentially jeopardize
the system stability.

### Wayland Session

To run a Wayland compositor, it requires an active systemd session and a
seat (i.e., `seat0`). The seat contains devices (e.g., `/dev/dri/card0`) that
the compositor will need to access. Furthermore, the session needs a
TTY associated.

This addon creates a new session using a simple service named `wayland_session`,
triggering a PAM login process on TTY7 (as it provides a graphical environment).
Then, systemd launches another containerized workload to activate the session.

## Weston

### Build image

Build the image featuring a Weston compositor by entering the project folder
and running:

```shell
$ AUTOSD_DEMO_ENV=weston autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 1.5G Jul 18 10:28 autosd-qemu-wayland-package-weston.x86_64.qcow2
```

### Run image

Use `automotive-image-runner` to run the image with QEMU:

```shell
$ automotive-image-runner autosd-qemu-wayland-package-weston.x86_64.qcow2
```

The window shall automatically pop up.

<div align="center">
    <img src="./img/weston.png" />
</div>

## Mutter

### Build image

Build the image featuring a Mutter compositor by entering the project folder
and running:

```shell
$ AUTOSD_DEMO_ENV=mutter autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 1.5G Jul 18 10:32 autosd-qemu-wayland-package-mutter.x86_64.qcow2
```

### Run image

Use `automotive-image-runner` to run the image with QEMU. Since Mutter requires
a DRM render node available, we need to specify additional options for QEMU
in the command line:

```shell
$ automotive-image-runner autosd-qemu-wayland-package-mutter.x86_64.qcow2 -device virtio-vga-gl -display dbus,gl=on
```

Note that we chose a DBus display. Feel free to change it to any other available
display type (e.g., `gtk`).

Then, you can run any DBus-specific viewer such as [Snowglobe](https://gitlab.gnome.org/bilelmoussaoui/snowglobe) to interact with the running VM:

```shell
$ snowglobe --show-host-cursor
```

The viewer window will appear.

<div align="center">
    <img src="./img/mutter.png" />
</div>
