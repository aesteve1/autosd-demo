# Socket IPC Demo

This example project allows two containers to communicate through a unix
domain socket and cross environments (ASIL-QM) in AutoSD.

## Architecture

This autosd-demo project features two containerized agents that communicate
between them through a unix domain socket. The socket is created using
[systemd.socket](https://www.freedesktop.org/software/systemd/man/latest/systemd.socket.html) feature, and then mounted into the respective containers.

On the one hand, the containerized service in the ASIL layer acts
as a listener, reading from the unix socket.
On the other hand, the containerized service in the
QM layer connects to the unix socket and sends time updates.

QM container Quadlet is accordingly updated to mount the volume
that contains the unix socket. To achieve that, the example employs
Quadlet's Drop-In feature.

> **_NOTE:_**  Podman version that is currently shipped with AutoSD does
not include Quadlet Drop-In feature, so it needs to update the installation
with a custom Copr repository.

## Usage

### Build image

Build the image by entering the project root folder and running:

```shell
$ autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 967M Apr 23 17:22 autosd-qemu-socket-ipc-package.x86_64.qcow2
```

### Run image

Now you can run the example by doing:

```shell
$ automotive-image-runner autosd-qemu-socket-ipc-package.x86_64.qcow2
```

QEMU window will pop up. You can log in as usual and check server side logs:
- `ipc.service`:

    ```
    ... localhost systemd[1]: ipc.service: Current time is: 10:43:02
    ```
