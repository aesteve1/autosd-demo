#!/usr/bin/env python3
import datetime
import os
import socket
import sys
import time

import psutil

PROCESS = psutil.Process(os.getpid())
PROCESS_CREATION_TIME = datetime.datetime.now().strftime("%X")
# A podman network IP address where the server is publishing its listener port.
HOST = "10.88.0.1"
PORT = 7001  # The port the server listens on

MiB = 1024**2
MEM_CHUNKS = []


class MemoryDump:
    def __init__(self):
        mem = psutil.virtual_memory()
        self.vms = PROCESS.memory_info().vms / MiB
        self.total = mem.total / MiB
        self.available = mem.available / MiB
        self.used = mem.used / MiB
        self.free = mem.free / MiB
        self.percent = mem.percent

    def __str__(self) -> str:
        return (
            f"MemoryDump ({PROCESS_CREATION_TIME}): "
            f"virtual = {self.vms}, total = {self.total}, "
            f"available = {self.available}, used = {self.used}, "
            f"free = {self.free}, percent = {self.percent}"
        )

    def to_bytes(self) -> bytes:
        return self.__str__().encode()


def consume_memory_chunk(chunk_size):
    MEM_CHUNKS.append(" " * chunk_size)


if __name__ == "__main__":
    chunk_size_mib = 1
    print("Initiating:", MemoryDump())
    print(f"Connecting to {HOST}:{PORT}")
    sys.stdout.flush()
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            try:
                sock.connect((HOST, PORT))
            except OSError as e:
                print("Connection failed:", e)
                print("Retrying in 5 seconds...")
                sys.stdout.flush()
                time.sleep(5)
                continue

            try:
                consume_memory_chunk(chunk_size_mib * MiB)
            except MemoryError:
                # A safeguard, in case QM policy failed to close the service.
                MEM_CHUNKS.clear()
                chunk_size_mib = 1
                continue
            print(f"Sending data (allocated {len(MEM_CHUNKS)} chunks)")
            sys.stdout.flush()
            sock.sendall(MemoryDump().to_bytes())
            chunk_size_mib += 1
            time.sleep(1)
