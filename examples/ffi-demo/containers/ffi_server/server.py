#!/usr/bin/env python3
import socket
import sys

PORT = 7001

if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((socket.gethostname(), PORT))
        sock.listen(1)
        while True:
            connection, addr = sock.accept()
            data = connection.recv(1024).decode()
            if data:
                print("Received from client:", data)
                sys.stdout.flush()
