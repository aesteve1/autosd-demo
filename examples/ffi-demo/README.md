# FFI Demo

This example project demonstrates how AutoSD effectively constrains the memory
usage in the QM layer, while avoiding any interference in the Safety layer.

The example includes performance monitoring features both embedded in the image,
and as external containers that can be brought up using compose.

## Architecture

This `autosd-demo` project features two containerized agents that communicate
between them through the TCP port `7001`, which is published to the host
on the container.

<div align="center">
    <img src="./img/diagram.png" />
</div>


### Listener agent

On the one hand, we have a listener agent that will remain actively
listening for incoming connections on the port `7001`.
The container resides in the Safety layer.

A podman network IP address where the server is publishing its listener
port is `10.88.0.1`, as can be verified the following way:

```
[root@localhost ~]# podman network inspect podman
[
     {
          "name": "podman",
          "id": "2f259bab93aaaaa2542ba43ef33eb990d0999ee1b9924b557b7be53c0b7a1bb9",
          "driver": "bridge",
          "network_interface": "podman0",
          "created": "2024-08-28T16:45:05.993141894Z",
          "subnets": [
               {
                    "subnet": "10.88.0.0/16",
                    "gateway": "10.88.0.1"
               }
          ],
...
```

The agent merely listens and prints the received memory dump data.
It shall not lose any packet, be interrupted, or killed by the supervisor.

### Malicious agent

On the other hand, we have an agent in the QM layer that will connect to the
listener in the Safety layer, start consuming chunks of memory, and send
its latest memory consumption data through the network.

Since the QM layer by default runs on a private network, need to declare
the client's container to run with `Network=host` configuration, so it will
be able to connect to the server.

Memory consumption of this service is limited to 50% of the available memory.

### Monitoring

The example includes a built-in monitoring system, with some services
adding, among others:
- Advanced logging
- [Redis](https://redis.io/) server
- [Performance Co-Pilot](https://pcp.io/) API server on the port 44322
- [Grafana](https://grafana.com/) container available on the host

## Expectations

The QM agent starts eating memory faster and faster. Consequently, its
service shall get killed and restarted by the supervisor, as it will
violate the memory restriction policy. This will keep happening in cycles.

Despite this memory-hungry process running, the agent in the Safety layer
shall remain unaffected, up and running.

## Usage

### Build image
Build the image by entering the project root folder and running:

```shell
$ autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 1228079104 Apr 11 15:17 autosd-qemu-ffi-demo-package.x86_64.qcow2
```

### Run image

Now you can run the example by doing:

```shell
$ automotive-image-runner --port-forward "44322:44322" autosd-qemu-ffi-demo-package.x86_64.qcow2
```

QEMU window will pop up, use `root` / `password` to log in.

You can check each agent's logs:
- `ffi_server.service`
  - Available in the Safety layer.
  - We can see all the logs for data coming from the client with no
    interruption of the service or data loss.
- `ffi_client.service`
  - You need to connect to the QM layer to see this service.
    ```shell
    $ podman exec -it qm bash
    ```
  - Logs show how the service is periodically killed and restarted.
    ```
    ...
    ... 35fc8b0b06d0 systemd[1]: ffi_client.service: A process of this unit has been killed by the OOM killer.
    ...
    ... 35fc8b0b06d0 systemd[1]: Stopped ffi_client container.
    ...
    ... 35fc8b0b06d0 systemd[1]: Starting ffi_client container...
    ...
    ... 35fc8b0b06d0 systemd[1]: Started ffi_client container.
    ...
    ```

### Run Grafana

Finally, we can attach the monitoring system to the running image on the host
by doing:

```shell
$ autosd-demo compose up -- -d --build
```

Verify that Grafana is running:

```shell
$ podman ps
CONTAINER ID  IMAGE                                COMMAND               CREATED       STATUS      PORTS       NAMES
0a247e5c2891  localhost/monitoring_grafana:latest  /bin/sh -c /usr/l...  21 hours ago  Up 21 hours              monitoring_grafana_1
```

In case of errors like `already exists` or `already in use`, try to execute:
```shell
$ autosd-demo compose down
```
and then re-run the monitoring system as described above.

Now you can just use a browser and open `localhost:3000` to get all system
data provided by the built-in monitoring add-on.
Use `admin` / `admin` to log in.

Swap memory graphics should show how the memory grows and gets emptied after
each consume cycle.

<div align="center">
    <img src="./img/screenshot.png" />
</div>
