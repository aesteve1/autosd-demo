# AutoSD Demo

[Website](https://gitlab.com/CentOS/automotive/src/autosd-demo)
|
[Getting started](https://centos.gitlab.io/automotive/src/autosd-demo/quick-start.html)
|
[Documentation](https://centos.gitlab.io/automotive/src/autosd-demo/)
|
[Contributing](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/blob/main/CONTRIBUTING.md)

## Why AutoSD Demo

AutoSD demo is a CLI tool for creating new AutoSD demos.

It was designed to be an easy ramp-up for both customers and developers.

## License

AutoSD Demo is released under the MIT License. See the [LICENSE](LICENSE) file for more details.

## Authors

- Roberto Majadas
