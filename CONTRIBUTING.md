# Contribution guidelines

Thank you for considering contributing to the `autosd-demo` project!

## How to report bugs
TBD

### Security issues
TBD

### Bug reports
TBD

### Features and enhancements
TBD

### Dependencies

#### pre-commit
Install a `pre-commit` framework:
```bash
$ sudo dnf install pre-commit
```
Execute from the `autosd-demo` root folder:
```bash
$ pre-commit install
```
This will install `pre-commit` into your git hooks,
and it will run automatically on every `git commit`.

If you want to manually run all `pre-commit` hooks on a repository, run:
```bash
$ tox -e pre-commit
```

More info can be found [here](https://pre-commit.com).

### Developer installation

Please read the "`Install from GitLab repository`" section of the [Installation guide](https://centos.gitlab.io/automotive/src/autosd-demo/install.html#option-1-install-from-gitlab-repository).



### Testing

If you want to enable testing requirements, use the following command:

```bash
$ pip install -r requirements/dev.txt
```

To run the tests, execute:

```bash
$ tox -e py
```

### Linters

To run linters code analysis, execute:

```bash
$ tox -e linters
```

### Code reviews
TBD

### Commit messages
TBD

### Commit access to the `autosd-demo` repository
TBD
